process.env.NODE_ENV = process.env.NODE_ENV || 'production';

// Customer settings file
const settings = require('./settings.json');

const	twilio        = require('twilio'),
		twilio_client = require('twilio')(settings.twilio.sid, settings.twilio.token),
		express       = require('express'),
		bunyan        = require('bunyan'),
		bodyParser    = require('body-parser'),
		helmet        = require('helmet');

// Bunyan logger
const log = bunyan.createLogger({
	name: 'smw_phone',
	streams: [{
		path: 'logs/all.log'
	}]
});

// Queue Cache handler
const QueueCache = require('./lib/queue_cache.js');
const queue_cache = new QueueCache(twilio_client);

queue_cache.on('update', (members, parameters) => {
	let data = {
		direction: parameters.direction,
		queue: queue_cache.queue
	};
	io.emit('queue', data);
});

queue_cache.on('update_current_calls', (current_calls) => {
	console.log(current_calls);
	io.emit('current_calls', current_calls);
});

// Express, Slack, and Socket.io conf
const app = express();
const server = app.listen(process.env.PORT || 3000);
const io = require('socket.io')(server);

io.on('connection', function (socket) {
	socket.emit('queue', {direction: null, 	queue: queue_cache.queue});
});

// Express routes
const route           = require('./routes/route.js');
const inbound_route   = require('./routes/inbound.js');
const outbound_route  = require('./routes/outbound.js');
const voicemail_route = require('./routes/voicemail.js');
const queue_route     = require('./routes/queue.js')(queue_cache);

// Express Middleware
app.use(helmet());
app.use(express.static('public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/', route);
app.use('/inbound', inbound_route);
app.use('/outbound', outbound_route);
app.use('/voicemail', voicemail_route);
app.use('/queue', queue_route);

// Express error handling
app.use((err, req, res, next) => {
	console.log(err);
	log.error({
		stack: err.stack,
		error: err
	}, 'Express error');
	res.status(500).send('Internal error');
});


/***********************************
// EXPRESS ROUTES
***********************************/

// Callback after calls
app.post('/after_call', (req, res) => {
	let twiml = new twilio.TwimlResponse();

	log.info({
		call_data: {
			call_sid:     req.query.CallSid,
			call_status:  req.query.DialCallStatus,
			caller:       req.query.Caller,
			duration:     req.query.DialCallDuration
		}
	}, 'Call ended');
	
	twiml.hangup();
	res.send(twiml.toString());
});

// Called after every call (callback)
app.post('/call_status', (req, res) => {
	res.sendStatus(200);
});
