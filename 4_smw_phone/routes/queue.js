module.exports = (queue_cache) => {
	const express = require('express');
	const router = express.Router();
	const twilio = require('twilio');
	const redis = require('redis');
	const moment = require('moment');
	const async = require('async');
	const settings = require('./../settings.json');

	// Method for dequeing call
	router.post('/dequeue', (req, res) => {
		let twiml = new twilio.TwimlResponse();
		let client = redis.createClient();

		let is_sip = req.body.From.includes('@smw-dev.sip.us1.twilio.com');
		let is_allowed = settings.allowed_dequeuers.indexOf(req.body.From);


		if(is_sip || is_allowed > -1)
		{

			client.hmset('dqr:'+ req.body.CallSid, {
				call_sid: req.body.CallSid,
				from: req.body.From,
				to: req.body.To,
				start_time: moment().format()
			}, (err, res) => {
				client.expire('dqr:'+ req.body.CallSid, settings.redis.cache_time);
			});

			twiml.say('Dequeuing');
			twiml.play('/audio/beep.mp3');
			twiml.dial({
				record: true,
				action: '/queue/recording_callback'
			}, (node) => {
				node.queue(settings.twilio.queue_name, {
					url: '/queue/dequeue_callback'
				});	
			});
		}
		else
		{
			twiml.say('Access is denied');
		}

		twiml.hangup();

		res.set('Content-Type', 'text/xml');
		res.send(twiml.toString());
	});

	// Called when caller gets put into queue and establishes wait
	router.post('/wait', (req, res) => {
		let twiml = new twilio.TwimlResponse();
		let client = redis.createClient();

		async.series([
			(callback) => {
				client.hmset('caller:'+ req.body.CallSid, {
					call_sid: req.body.CallSid,
					from: req.body.From,
					to: req.body.To,
					start_time: moment().format()
				},() => {
					callback();
				});
			},
			(callback) => {
				client.expire('caller:'+ req.body.CallSid, settings.redis.cache_time);
				callback();
			},
		], (err, result) => {
			client.quit();
		});

		settings.hold_music.sort(function() { return 0.5 - Math.random() });

		queue_cache.update('in');

		settings.hold_music.forEach((song) => {
			twiml.play(song);
		});

		res.set('Content-Type', 'text/xml');
		res.send(twiml.toString());
	});

	// Called after call in queue hangs up or after bridged call
	router.post('/result', (req, res) => {
		let twiml = new twilio.TwimlResponse();

		console.log(req.body.QueueResult);
		if(req.body.QueueResult == 'hangup' || req.body.QueueResult == 'bridged')
		{
			queue_cache.update();
		}

		twiml.hangup();
		res.set('Content-Type', 'text/xml');
		res.send(twiml.toString());
	});

	// Called after dequeing call
	router.post('/recording_callback', (req, res) => {
		let twiml = new twilio.TwimlResponse();
		twiml.hangup();
		res.set('Content-Type', 'text/xml');
		res.send(twiml.toString());
	});

	// Method for getting queue details
	router.get('/view', (req, res) => {
		twilio_client.queues(settings.twilio.queue_sid).members.list((err, data) => {
			let members = data.queue_members;
			let return_data = [];

			async.eachOf(members, (member, key, callback) => {
				let now = moment();
				let call_time = moment(member.date_enqueued);
				let sec = now.diff(call_time, 'seconds');
				let min = Math.floor(sec/60);

				let calculated_time = min +'m '+ (sec % 60) +'s';

				return_data.push({
					call_sid: member.call_sid,
					position: member.position,
					date_enqueued: member.date_enqueued,
					calculated_time: calculated_time
				});

				twilio_client.calls(member.call_sid).get((err, call) => {
					if(err) return callback(err);

					return_data[key].from = call.from;
					callback();
				});
			},
			(err) => {
				if(err) 	
					console.log(err);
				else
					res.json(return_data);
			});
		});
	});

	// Called after dequeueing
	router.post('/dequeue_callback', (req, res) => {
		let twiml = new twilio.TwimlResponse();
		let client = redis.createClient();

		client.hmset('caller:'+ req.body.CallSid, {
			dequeuer: req.body.DequeingCallSid
		},(err, res) => {
			queue_cache.update();
		});
		
		res.set('Content-Type', 'text/xml');
		res.send(twiml.toString());
	});


	return router;
}

