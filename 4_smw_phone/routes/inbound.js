const express = require('express');
const router = express.Router();
const twilio = require('twilio');
const moment = require('moment-timezone');
const settings = require('./../settings.json');

// Main inbound route /inbound
router.all('/', (req, res) => {
	const twiml = new twilio.TwimlResponse();
	const holidays = settings.holidays;

	let vm;
	let holiday;
	let holiday_name;

	let today = moment.utc().tz('America/Los_Angeles');
	let day = today.isoWeekday();


	// function for iterating through holiday array items and checking for holiday
	const check_holiday = (arr) => {
		let result = arr.filter((item) => {
			let [month, day] = item.date;
			return (month == today.month() + 1) && day == today.date();
		});

		if(result.length > 0)
			return result[0];
		else
			return false;
	}

	// check for holiday and set hours
	if(item = check_holiday(holidays['every_year']))
	{
		holiday = true;
		holiday_name = item.name;
		[start_hour, end_hour] = item.hours;
	}
	else if(item = check_holiday(holidays[today.year()]))
	{
		holiday = true;
		holiday_name = item.name;
		[start_hour, end_hour] = item.hours;
	}
	// set Saturday schedule
	else if(day == 6)
		[start_hour, end_hour] = settings.hours.saturday;
	// set Sunday schedule
	else if(day == 0)
		[start_hour, end_hour] = settings.hours.sunday;
	// set default
	else
	{
		[start_hour, end_hour] = settings.hours.weekdays;
	}

	// check if currently within operating hours
	if(today.hour() >= start_hour && today.hour() < end_hour)
		vm = false;
	else
		vm = true;

	// if holiday then play a message with the custom hours
	if(holiday)
	{
		let hours_msg;

		if(start_hour == end_hour)
			hours_msg = 'closed today';
		else
			hours_msg = `open from ${start_hour > 12 ? (start_hour - 12) + 'pm' : start_hour + 'am'} to ${end_hour > 12 ? (end_hour - 12) + 'pm' : end_hour + 'am'}`;

		twiml.say('Thank you for calling Saddle Mountain Wireless.  In observance of '+ holiday_name +' our office will be '+ hours_msg +'.');
	}

	// deliver s3 redirect for xml files
	if(!vm)
	{
		twiml.play('http://static.smw.ninja/phone/audio/inbound-english.mp3');
		twiml.enqueue(settings.twilio.queue_name,{
			action: '/queue/result',
			waitUrl: '/queue/wait'
		});
	}
	else
		twiml.redirect({method: 'GET'}, 'http://static.smw.ninja/phone/xml/english-vm.xml');

	res.set('Content-Type', 'text/xml');
	res.send(twiml.toString());
});

module.exports = router;
