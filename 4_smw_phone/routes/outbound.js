const express = require('express');
const router = express.Router();
const twilio = require('twilio');
const settings = require('../settings.json');

// Route for Outbound calling and dequeuing from SIP client by dialing 1
router.post('/', (req, res) => {
	let twiml = new twilio.TwimlResponse();
	let number = req.body.To;
	let phone_regex = /^1?([2-9]\d\d)([2-9]\d\d)(\d\d\d\d)$/;

	// Remove SIP info from phone number
	if(number && number != 'sip:1@smw-dev.sip.us1.twilio.com' && number.includes('@'))
	{
		number = number.replace('sip:', '');
		number = number.substr(0, number.indexOf('@'));
	}

	// If number dialed == 1 then dequeue next caller
	if(number == 'sip:1@smw-dev.sip.us1.twilio.com'){
		twiml.redirect('/queue/dequeue');
	}
	// Validate phone number format and dial
	else if(phone_regex.test(number)){
		number = (number.length == 10) ? '+1'+ number : '+'+ number;

		twiml.dial({
			// callerId: '+18888341145'
			callerId: settings.outbound_caller_id
		}, (node) => {
			node.number(number);
		});
	}
	// Return invalid number
	else
	{
		twiml.say('Invalid phone number');
		twiml.hangup();
	}

	res.set('Content-Type', 'text/xml');
	res.send(twiml.toString());
});

module.exports = router;
