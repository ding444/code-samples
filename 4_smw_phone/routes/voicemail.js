const express = require('express');
const router = express.Router();
const twilio = require('twilio');
const Slack = require('node-slack');
const settings = require('./../settings.json');

const slack = new Slack(settings.slack_hook_url);

// Callback for after voicemail recording /voicemail
router.post('/', (req, res) => {
	// log.info({query: req.body}, 'voicemail request');
	let caller = req.body.From ? req.body.From : req.body.Caller;

	slack.send({
		text: ' ',
		channel: '#phone',
		username: 'SMW Phone',
		icon_emoji: ':telephone_receiver:',
		// unfurl_links: false,
		text: 'New voicemail from '+ caller,
		attachments: [{
			text: '_'+ req.body.TranscriptionText +'_',
			mrkdwn_in: ['text', 'pretext'],
			fields: [{
				short: true,
				title: 'Caller',
				value: caller
			},{
				short: true,
				title: 'Recording link',
				value: '<'+ req.body.RecordingUrl +'.mp3| Listen to recording>'
			}]
		}],
	});

	res.sendStatus(200);
});

// Gather digits for voicemail handling
router.get('/gather', (req, res) => {
	let twiml = new twilio.TwimlResponse();
	// log.info({
	// 	call_data: {
	// 		call_sid: req.query.CallSid,
	// 		caller: req.query.Caller,
	// 		digits: req.query.Digits
	// 	}
	// }, 'Gather input received');

	if(req.query.Digits == '1')
		twiml.redirect({method: 'GET'}, 'http://static.smw.ninja/phone/xml/record.xml');
	else
		twiml.redirect({method: 'GET'}, 'http://static.smw.ninja/phone/xml/spanish-vm.xml');
		

	res.set('Content-Type', 'text/xml');
	res.send(twiml.toString());
});

module.exports = router;
