# Saddle Mountain Wireless Phone Queue
---

This project is what handles SMW calling using Twilio.  It runs on a Node.js backend and its entry point is `index.js`.  It uses Twilio for handling phone calls.


## Task list

- [ ] Launch `V2` branch
- [ ] Add queue callback functionality
- [ ] Cache queue to minimize Twilio API calls
- [ ] Write some alarms
	- [ ] Excessive callers in queue
	- [ ] Long hold times
- [ ] Add metrics
- [ ] Add Twilio js client
- [ ] Transfer call functionality
	- _Maybe parking lots?_
- [ ] Offer a number for them to text for information on outages
	- "We are having outages in --------- for more information please press N"
	- "We are having outages in --------- to receive a text with more information please press N"
	- "We are having outages in --------- to be texted updates press N"


## Installation

This Node project uses some ES6 syntax features so make sure your version of Node is compatible.  It is also dependent on Redis for some call caching to minimize API calls to Twilio.

```sh
git clone https://gitlab.com/ding444/smw-node-phone
```

Be sure to install dependencies with:

```sh
npm install
```

## Usage

There are settings in `./settings.json` that handle things like Twilio API keys, specifying holidays and hold music, and authentication settings.

Usage is pretty simple.  You can simply launch it like:

```sh
PORT=3000 node index.js
```

## Credits


TODO: Write credits


## License

There currently is no license and this is *not* available for distribution.  **No permissions is given to use any part of this project**.
