const EventEmitter	= require('events');
const async = require('async');
const redis = require('redis');
const settings = require('./../settings.json');

class QueueCache extends EventEmitter {

	constructor(twilio_client) {
		super();
		this.twilio_client = twilio_client;
		this.members = [];
		this.curr_calls = [];
	}

	get queue() {
		//return this.update('out');
		return this.members;
	}

	get current_calls() {
		return this.curr_calls;
	}

	update(direction = 'out') {
		let client = redis.createClient();
		let callers = [];
		let twilio_client = this.twilio_client;

		twilio_client.queues(settings.twilio.queue_sid).members.list((err, data) => {

			async.each(data.queue_members, (member, callback) => {
				//TODO: get call_sid details from redis
				client.hgetall('caller:'+ member.call_sid, (err, res) => {
					if(err) console.log(err);
					let call_info = res;

					callers.push({
						position: member.position,
						from: call_info.from,
						call_sid: member.call_sid,
						date_enqueued: member.date_enqueued
					});

					callback();
				});
			}, (err) => {
				if(err) console.log(err);
				callers.sort((a,b) => {
					if(a.position < b.position)
						return -1;
					if(a.position > b.position)
						return 1;
					return 0;
				});
				this.members = callers;

				this.emit('update', this.members, {direction: direction});
				// client.quit();
			});
		});


		let current_calls = [];
		twilio_client.calls.list({status: 'in-progress'}, (err, data) => {
			async.each(data.calls, (call, callback) => {
				let {sid} = call;

				client.hgetall('caller:'+ sid, (err, caller) => {
					if(err) console.error(err);

					if(caller !== null && caller.dequeuer)
					{
						client.hgetall('dqr:'+ caller.dequeuer, (err, dqr) => {
							if(err) console.error(err);

							let push_data = {
								caller_number: caller.from,
								receiving_agent: dqr.from,
								start_time: dqr.start_time
							}

							current_calls.push(push_data);
							callback();

						});
					}
					else
					{
						callback();
					}
				});
			}, (err) => {
				this.curr_calls = current_calls;
				this.emit('update_current_calls', this.current_calls);
			});
		});

	}
}

module.exports = QueueCache;
