# SMW Phone

## Introduction

This is a phone system replacement for Saddle Mountain Wireless.  The evolution of this project lead us from using landlines for our customer service department, to voip and SIP servers, and eventually this.  Our customer service reps use a SIP client or an approved phone to call in and dequeue customer calls that get enqueued in Twilio.  This system took us from $1,200/mo in landline fees to $180/mo with over double the minutes used.  It all resides in an AWS EC2 instance so it also enables our reps to connect up from anywhere they have phone or internet service.

## Why I think it's cool

This project interfaces with Twilio's APIs and communicates through a combination of XML HTTP and API requests. It's built on Node.js.  This is an incomplete version and is in a transition to use redis as a job queue and information cache with plans to implement logging and metric tracking in MongoDB.

Voicemails from this system are transcribed and sent to a Slack channel that we use.
