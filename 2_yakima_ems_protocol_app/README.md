# Yakima EMS Protocol App

## Introduction

This was a personal project that took Yakima County EMS Protocols that are the patient care guidelines and requirements for Paramedics and EMTs.  I am a volunteer EMT for our fire department and got frustrated with having to use a massive 3-ring binder or dig through PDF files any time I needed to look something up.

This app makes looking information up quick and simple and adds search functionality.  It is a little old (2 yrs) and I haven't done a lot to maintain it because they keep telling us new protocols will be released soon.


## Why I think it's cool

This project is built using the [Ionic Framework](https://ionicframework.com/) which uses AngularJS for the front-end code.  I only included the `/www` folder of the framework in order to avoid needless files.  This app will compile and run easily on iOS and Android devices.  It doesn't include any real back-end functionality.
