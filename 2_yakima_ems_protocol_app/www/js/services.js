angular.module('protocols.services', [])

.factory('Protocols', function($http){
	return {
		get_protocol: function(protocol){
			return $http.get('json/protocols/'+ protocol +'.json', {cache: true});
		}
	}
})

.factory('Menu', function($http){
	return {
		get: function(menu){
			return $http.get('json/menu.json', {cache: true});
		}
	}
});
