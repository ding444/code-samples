// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers','protocols.services', 'ngCordova'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  .state('app', {
    url: "/app",
    abstract: true,
    templateUrl: "templates/menu.html",
    controller: 'AppCtrl'
  })


  .state('app.browse', {
    url: "/browse",
    views: {
      'menuContent': {
        templateUrl: "templates/browse.html"
      }
    }
  })
    .state('app.sub-menu', {
		resolve: {
			menu: function($q, Menu, $stateParams){
				var deferred = $q.defer();
				Menu.get($stateParams.menu).success(function(data){
					deferred.resolve(data[$stateParams.menu]);
				});
				return deferred.promise;
			}
		},
		url: "/submenu/:menu",
			views: {
			'content': {
				templateUrl: "templates/sub-menu.html",
				controller: 'MenuCtrl'
			}
		}
    })
	.state('app.protocol', {
		resolve: {
			protocol: function($q, Protocols, $stateParams){
				var deferred = $q.defer();
				Protocols.get_protocol($stateParams.page).success(function(data){
					deferred.resolve(data);
				});
				return deferred.promise;
			}
		},
		url: '/protocol/:page',
		views: {
			'content': {
				templateUrl: 'templates/protocol.html',
				controller: 'ProtocolCtrl'
			}
		}
	})

	.state('app.start', {
		url: "/start",
		views: {
			'content': {
				templateUrl: "templates/start.html"
			}
		}
	});
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/start');
});
