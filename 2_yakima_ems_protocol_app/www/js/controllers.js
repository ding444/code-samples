angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

})

.controller('MenuCtrl', function($scope, menu) {
	// Menu.get().success(function(data){
	// 	$scope.menu = data[$stateParams.menu];
	// })
	// $scope.menu = menus[$stateParams.menu];
	$scope.menu = menu;
})

.controller('ProtocolCtrl', function($scope, protocol, $ionicModal) {
	$scope.protocol = protocol;

	$scope.run = function(){
		// $cordovaInAppBrowser.open('pdfs/YC Protocols Table of Contents Updated 2013.pdf', '_blank')
		// 	.then(function(event) {
		// 		// success
		// 	})
		// 	.catch(function(event) {
		// 		// error
		// 	});
		$scope.modal.show();
	}

	$ionicModal.fromTemplateUrl('templates/image_modal.html', {
		scope: $scope,
		animation: 'slide-in-up'
	}).then(function(modal) {
		$scope.modal = modal;
	});

})

.controller('PlaylistCtrl', function($scope, $stateParams) {
});
