var XLSX		= require('xlsx'),
	tunnel		= require('tunnel-ssh'),
	mysql		= require('mysql'),
	async		= require('async'),
	sprintf		= require('sprintf-js').sprintf,
	prompt		= require('prompt'),
	distance	= require('jaro-winkler'),
	xls_utils	= XLSX.utils,
	json2csv	= require('json2csv');

prompt.message = '';
prompt.delimiter = '';

// Unlock SSH key
prompt.get([
	{
		name: 'passphrase',
		description: 'SSH Key Passphrase: ',
		hidden: true
	}
], function(err, results){
  if(err) console.log(err);
	var passphrase = results.passphrase

  // Open spreadsheet
  // TODO: Change this to cli parameter
	var workbook = XLSX.readFile('./2016 12 Saddle Mountain All Services.xls');
	var sheet = workbook.Sheets[workbook.SheetNames[0]];

	var num_rows = xls_utils.decode_range(sheet['!ref']).e.r;

	var rows_to_check = [];

  // Iterate through rows and compare values
	for(var i = 0, l = num_rows; i < l; i++){
		var cell = xls_utils.encode_cell({c:1, r:i});
		var value = sheet[cell];

		if(value === undefined) continue;

		switch(value.v){
			case 'Basic Access Fee':
			case 'Premium Access Fee': 
				var data = {
					row: i,
					zaccount: sheet[xls_utils.encode_cell({c: 2, r: i})].v,
					name: sheet[xls_utils.encode_cell({c: 3, r: i})].v,
					address: sheet[xls_utils.encode_cell({c: 4, r: i})].v,
					city: sheet[xls_utils.encode_cell({c: 5, r: i})].v,
				};
				rows_to_check.push(data);
				break;
			default:
				break;
		}
	}

  // SSH Tunnel config
	var tunnel_config = {
		host: '208.90.161.200',
		username: 'root',
		dstPort: 3306,
		localPort: 3307,
		privateKey: require('fs').readFileSync('./keys/id_rsa'),
		passphrase: passphrase
	};

  // Make SSH connection
	var server = tunnel(tunnel_config, function(error, result){
		if(error) console.log(error);

    // Connect to SSH over SSH forwarded port
		var connection = mysql.createConnection({
			host: 'localhost',
			port: 3307,
			user: 'script_user',
			password: 'mhL11(Q4$h.c)t2$M`RrG2)DKMgU2Jpg]n$Bg8YBFuu|B2q23l',
			database: 'powernoc',
			insecureAuth: true
		});

		connection.connect(function(err){
			if(err) console.log(err);
		});
		
		// Set up counters for stats at end
		var errors = {
			invalid: {
				name: 'Invalid ZACCOUNTs in PC',
				columns: ['pud_name', 'pud_address', 'zaccount'],
				data: []
			},
			not_active: {
				name: 'Accounts that are not active',
				columns: ['customer_id', 'customer_name', 'customer_status', 'zaccount'],
				data: []
			},
			different_name: {
				name: 'Accounts with different names',
				columns: ['customer_id', 'pc_name', 'pud_name', 'score'],
				data: []
			},
			different_address: {
				name: 'Accounts with different addresses',
				columns: ['customer_id', 'pc_address', 'pc_city', 'pud_address', 'pud_city', 'score'],
				data: []
			},
		};

    // Validate ZACCOUNT in db
		async.each(rows_to_check, function(item, callback){
			var zaccount = item.zaccount;
			var zaccount_formatted = parseInt(zaccount.replace('ZACCOUNT', ''));

			connection.query('SELECT * FROM CustomerMoreInfo AS a LEFT JOIN Customer AS b ON a.CustomerID = b.CustomerID WHERE a.CustomInfoTemplateID = ? AND a.Value = ?', [45, zaccount_formatted], function(error, result){
				if(error) console.log(error);
				if(result.length < 1){
					errors.invalid.data.push({
						pud_name: item.name,
						pud_address: item.address +', '+ item.city,
						zaccount: item.zaccount
					});
					callback();
				}
				else{
					// COMPARE NAME AND MAKE SURE ACTIVE
					// Check status
					var customer = result[0];
					if(customer.Status !== 'Active')
					{
						errors.not_active.data.push({
							customer_id: customer.CustomerID,
							customer_name: customer.CompanyName,
							customer_status: customer.Status,
							zaccount: item.zaccount
						});
					}

					//Check name
					var customer_name = customer.LastName.trim() +', '+ customer.FirstName.trim();
					var name_score = distance(customer_name, item.name);
					var company_name_score = distance(customer.CompanyName, item.name);
					var score = name_score > company_name_score ? name_score : company_name_score;
					if(score < .8)
					{
						errors.different_name.data.push({
							customer_id: customer.CustomerID,
							pc_name: customer_name,
							pc_company_name: customer.CompanyName,
							pud_name: item.name,
							score: score
						});
					}

					//check address
					connection.query('SELECT * FROM Address WHERE CustomerID = ? AND Type = ?', [customer.CustomerID, 'Home'], function(err, addresses){
						var physical_address = addresses[0].Address1;
						var address_score = distance(physical_address, item.address);
						if(address_score < .9)
						{
							errors.different_address.data.push({
								customer_id: customer.CustomerID,
								pc_address: addresses[0].Address1,
								pc_city: addresses[0].City,
								pud_address: item.address,
								pud_city: item.city,
								score: address_score
							});
						}
						callback();
					});
				}
			});
		}, function(err){
      // Compile CSV output
			if(err) console.log(err);
			connection.end();
			var csv = '';
			[errors.invalid, errors.not_active, errors.different_name, errors.different_address].forEach(function(group){
				csv += group.name +',\n',
				csv += json2csv({
					data: group.data,
					fields: group.columns
				});
				csv += '\n,,\n,,\n';
			});

      // TODO: programatically create and dump to CSV
			console.log(csv);
			console.log('\n\n');

      // Output summary
			console.log('SUMMARY');
			console.log('-------------------');
			console.log('Accounts with no ZACCOUNT in PC: '+ errors.invalid.data.length);
			console.log('Accounts that are not active in PC: '+ errors.not_active.data.length);
			console.log('Accounts with different name:'+ errors.different_name.data.length);
			console.log('Accounts with different address:'+ errors.different_address.data.length);
		});
	});
});
