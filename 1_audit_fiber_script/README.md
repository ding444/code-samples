# Audit Fiber Script

# Introduction

This script audits a spreadsheet of wholesale internet accounts we get from the wholesaler and compares it against our CRM/billing database.  It keeps us from having to manually compare 1,500 accounts, saving us 6-8 man hours every month.  It also does some other analysis to ensure that we aren't being billed for accounts that are no longer active.

The script is a little old and by no means a work of art.  It could stand to be refactored and improved.  I currently use more ES6 syntax than demonstrated in this simple project.

# Why I think it's cool

Our billing server is secured and locked down in order to prevent a number of attacks which is great.  However, it makes it complicated when it comes to doing custom integrations.  In this scenario we want access to the MySQL database without having to mess with additional firewall rules and without having to compromise security.  This script uses a password-protected SSH key (removed from the project folder) to make an SSH connection to the billing server and forward port 3306 (MySQL) to the local machine so that the MySQL daemon can be connected to.  This creates temporary access over a _very_ secure SSH tunnel.

This script does some string comparisons on the customer names and address that our wholesaler has against what information we have.  I used to use a levenshtein distance algorithm but switched to a jaro-winkler for performance and (in my opinion) a better indication of how different the strings are so that we can make sure our records match.

