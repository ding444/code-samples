import GVRequestHelper from '../utils/gv_requests';
import {
  Silo,
  Hub,
  Device,
  GVDevice,
  GVDeviceType,
  GVResponses,
} from '../models';

class GVLinkController {

  constructor() {
    // Bind these methods
    this.build_tree = this.build_tree.bind(this);
    this.get_tree = this.get_tree.bind(this);
    this.make_generic_request = this.make_generic_request.bind(this);
  }


  /*****************************************
   * This makes a gv_request for  GVLink to
   * scan and fill the gv_devices colum
  *****************************************/
  get_tree(req, res) {
    // Make request
    GVRequestHelper.get_tree({
      silo: 0,
      hub: 0,
      device: 0,
    })

    // Scan for and retrieve request or timeout
    .then((request) => this.scanForResponse(request))

    // Send data
    .then((data) => {
      res.json({
        code: 200,
        status: 'success',
        message: null,
        error: null,
        data,
      });
    })

    // Error handling
    .catch((error) => this.sendError(res, error));
  }


  /*****************************************
   * This is a reusable function for handling
   * lots of get/post requests for GVLink devices
  *****************************************/
  make_generic_request(req, res, next) {
    // validate method exists in GVRequestHelper
    const method = req.method.toLowerCase() +'_'+ req.path.split('/')[1];

    // If function doesn't exist 404
    if (typeof GVRequestHelper[method] !== 'function') {
      res.status(404).send('Not found');
      return;
    }

    // Destructure ids
    const { 0:silo, 1:hub, 2:device } = req.params;
    let request = null;

    // Create GVLink data format for posts
    if (req.method === 'POST') {
      // Convert JSON key value to `KEY` VALUE, string
      request = Object.keys(req.body).map((key) => {
        // Capitalize key
        const key_txt = key.toUpperCase();

        // Caplitalize if string
        const value_txt = (typeof req.body[key] === 'string') ? req.body[key].toUpperCase() : req.body[key];

        return `\`${key_txt}\` ${value_txt}`;
      }).join(', ');
    }

    // Make request
    GVRequestHelper[method]({
      silo,
      hub,
      device,
      request,
    })

    .then((request) => this.scanForResponse(request))

    .then((data) => {
      const return_data = {
        code: 200,
        status: 'success',
        message: null,
        error: null,
        data,
      };

      // If request doesn't match
      if (data.response.request_status !== 1) {
        return_data.status = 'fail';
        return_data.code = 404;
        return_data.message = 'Error received in GVLink response'
        return_data.error = {
          type: 'GVLink error',
          name: 'Unmatching response',
        }
      }

      // send data
      res.json( return_data );
    })

    .catch((error) => this.sendError(res, error));
  }


  /*****************************************
   * This builds the device tree from data after
   * GVLink creates gv_devices
  *****************************************/
  build_tree(req, res) {
    // Get all devices and sort
    GVDevice.findAll({
      include: GVDeviceType,
      order: [
        [ 'silo', 'ASC'  ],
        [ 'hub',  'ASC' ],
        [ 'type',  'ASC' ],
        [ 'device',  'ASC' ],
      ],
    })

    // Create silos in db
    .then((devices) => {
      // Get silos from list of devices
      const silos = devices.filter((device) => device.type === 40);

      // Promise wrapper for all silo promises
      return new Promise((resolve, reject) => {

        // ForEach silo: create if doesn't exist
        Promise.all(silos.map((silo) =>
          Silo.findOrCreate({
            where: { MAC: silo.MAC },
            defaults: {
              order: silo.silo,
              status: silo.status,
              label: `Silo ${silo.silo--}`,
            }
          })
        ))

        // Array of silos
        .then((silos) => {
          // Get silo objects from array
          silos = silos.map((silo) => silo[0]);

          // Remove silos from devices
          const hubs_and_devices = devices.filter((device) => device.type !== 40);

          // Re-map silo_ids for devices
          const updated_devices = hubs_and_devices.map((device) => {
            // Get new silo_id
            const silo_id = silos.filter((silo) => silo.order == device.silo)[0].id;
            device.silo = silo_id; // Replace silo order with silo_id

            return device;
          });

          // Resolve with updated hubs and devices
          resolve(updated_devices);
        })

        .catch((error) => this.sendError(res, error));

      })
    })

    // Create hubs in db
    .then((devices) => {
      // Get hubs from list of devices
      const hubs = devices.filter((device) => [ 41, 45, 46 ].includes(device.type));

      // Promise wrapper for all hub promises
      return new Promise((resolve, reject) => {

        // ForEach hub: create if doesn't exist
        Promise.all(hubs.map((hub) =>
          Hub.findOrCreate({
            where: { MAC: hub.MAC },
            defaults: {
              silo_id: hub.silo,
              order: hub.hub,
              status: hub.status,
              label: `Hub ${hub.hub--}`,
            },
          })
        ))

        // Continue with array of hubs
        .then((hubs) => {
          // Get hub objects from array
          hubs = hubs.map((hub) => hub[0]);

          // Remove hubs from devices
          const just_devices = devices.filter((device) => ![ 41, 45, 46 ].includes(device.type));

          // Update hub_id of remaining devices
          const new_devices = just_devices.map((device) => {
            // Get hub id
            const hub_id = hubs.filter((hub) =>
              // Get hub_id where device silo_id and hub.order match
              hub.order == device.hub && hub.silo_id == device.silo
            )[0].id;
            device.hub = hub_id; // Replace hub order with hub_id

            return device;
          });

          resolve(new_devices);
        })

        .catch((error) => this.sendError(res, error));

      })
    })

    // Create devices in db
    .then((devices) => {
      // Promise wrapper for device promises
      return new Promise((resolve, reject) => {

        // Try creating devices
        Promise.all(devices.map((device) =>
          Device.findOrCreate({
            where: { MAC: device.MAC },
            defaults: {
              hub_id: device.hub,
              device_type_id: device.type,
              order: device.device,
              status: device.status,
              reading: device.reading,
            },
          })
        ))

        // Done -> continue
        .then((devices) => {
          resolve(devices);
        })

        .catch((error) => this.sendError(res, error));

      });
    })

    // Finished! exit
    .then((devices) => {
      res.status(200).json({
        code: 200,
        status: 'success',
        message: 'Tree built successfully',
        error: null,
        data: null,
      })
    })

    // Catch any errors and output to console
    .catch((error) => this.sendError(res, error));
  }


  /*************************************************************
   _   _ _   _ _ _ _          ___             _   _
  | | | | |_(_) (_) |_ _  _  | __|  _ _ _  __| |_(_)___ _ _  ___
  | |_| |  _| | | |  _| || | | _| || | ' \/ _|  _| / _ \ ' \(_-<
   \___/ \__|_|_|_|\__|\_, | |_| \_,_|_||_\__|\__|_\___/_||_/__/
                       |__/

   *************************************************************/


  /*****************************************
   * This is a utility function that takes a
   * gv_request and waits for a a gv_response to
   * be created by GVLink.
   *
   * It returns a refreshed original request
   * and the assiated response
  *****************************************/
  scanForResponse(request, max_attempts = 100, interval_ms = 100) {
    return new Promise((resolve, reject) => {
      let count = 0;

      // Start interval timer
      const timer = setInterval(() => {
        // Check for gv_response
        GVResponses.findOne({
          where: { gv_request_id: request.id },
        })

        .then((response) => {
          // If reached limit clear interfal and throw timeout error
          if(count === max_attempts) {
            clearInterval(timer);
            throw Error('Timed out at '+ count +' tries');
          }

          // If response exists clear and send request + response
          if (response !== null) {
            // Clear timer
            clearInterval(timer);

            // Return response
            request.reload().then((request) => {
              resolve({ request, response });
            });
          }

          count ++;
        })

        // Reject on error
        .catch((error) => {
          reject(error);
        });

      }, interval_ms);
    });
  }

  /*****************************************
   * This is a utility funciton that sends
   * standardized error responses
  *****************************************/
  sendError(res, error) {
    console.log(error);
    res.status(400).json({
        code: 500,
        status: 'fail',
        message: error.message,
        error: {
          type: 'general error',
          name: error.name,
        },
        data: null,
    });
  }
}

export default new GVLinkController();
