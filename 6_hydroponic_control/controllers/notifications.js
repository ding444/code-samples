import {
  Notification
} from '../models';

class NotificationsController {

  constructor() {
    // Bind these methods
    this.get = this.get.bind(this);
  }

  get(req, res) {
    const where = {};

    // Add id to select singular record when present
    if(req.params.id !== undefined) {
      where.id = req.params.id;
    }

    // Get silo(s)
    Notification.findAll({
      where,  // Use where object created above
    })

    // Success
    .then((notifications) => {
      if (notifications.length < 1) {
        // If no notifications 404 and error
        res.status(404).json({
          status: 404,
          message: `No notifications by id ${req.params.id}`,
        })
      } else {
        //  Success: send notification(s)
        res.status(200).json({
          status: 200,
          notifications: req.params.id === undefined ? notifications : notifications[0],
        });
      }
    })

    // Error
    .catch((error) => {
      console.error(error);
      res.status(400).json({
        status: 400,
        error: error.message
      });
    });

  }
}

export default new NotificationsController();
