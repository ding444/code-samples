// Agregate all controllers into one module
import RecipeController from './recipes';
import CropController from './crops';
import SiloController from './silos';
import NotificationsController from './notifications';
import GVLinkController from './gvlink';

export {
  RecipeController,
  CropController,
  SiloController,
  NotificationsController,
  GVLinkController,
}
