import {
  Silo,
  Hub,
  Crop,
  Recipe,
  Device,
  DeviceType,
  Notification,
  SiloHVAC,
  HVACType,
} from '../models';

class SiloController {

  constructor() {
    // Bind these methods
    this.get = this.get.bind(this);
    this.getPods = this.getPods.bind(this);
    this.getNotifications = this.getNotifications.bind(this);
    this.getDevices = this.getDevices.bind(this);
    this.getDeviceByType = this.getDevicesByType.bind(this);
  }

  get(req, res) {
    const where = {};

    // Add id to select singular record when present
    if(req.params.id !== undefined) {
      where.id = req.params.id;
    }

    // Get silo(s)
    Silo.findAll({
      include: [
        {
          model: Hub,                      // Join to hub
          include: [
            {
              model: Crop,
              attributes: [ 'id' ],          // Get these columns
              through: { attributes: [] },   // Don't worry about crop_has_hub
              where: { completed_at: null }, // Ignore completed crops
              include: {
                model: Recipe,
                attributes: [ 'name' ],
              },
              required: false,               // Include hubs without crops
            },
            {
              model: Device,
              include: [
                {
                  model: Notification,
                  required: false,
                  where: { status: { $ne: 'done' }},
                },
              ],
            },
          ],
        },
        {
          model: SiloHVAC,
          include: HVACType,
        }
      ],
      where,                             // Use where object created above
    })

    // Success
    .then((silos) => {
      const silo_data = silos.map((silo) => {
        const { id, label, order, status, MAC } = silo;

        const data = { id, label, order, status, MAC };

        // Sum pod data
        data.total_pods = silo.hubs.length;
        data.pods_in_use = silo.hubs.reduce((acc, value) =>
          value.crops.length > 0 ? acc + 1 : acc
        , 0);

        data.pods_available = data.total_pods - data.pods_in_use;

        // Preset notifications
        data.notifications = 0;

        // increment notifications for each
        silo.hubs.forEach((hub) =>
          hub.devices.forEach((device) =>
            data.notifications += device.notifications.length
          )
        );

        // Get temperatures
        const hvac_devices = silo.hubs.reduce((acc, hub) => {
          // Add devices that match id 83 (air temp) to array and return
          const temps = hub.devices.filter((device) => device.device_type_id === 83);
          const humidities = hub.devices.filter((device) => device.device_type_id === 86);
          const airs = hub.devices.filter((device) => device.device_type_id === 87);
          acc.temp = acc.temp.concat(temps);
          acc.humidity = acc.humidity.concat(humidities);
          acc.air = acc.air.concat(airs);

          return acc;
        }, { temp: [], humidity: [], air: [] });

        // Sum devices
        let temp_sum, temp_avg, humidity_avg, humidity_sum, air_sum, air_avg;

        // Calculate temperatures
        if (hvac_devices.temp.length > 0) {
          temp_sum = hvac_devices.temp.reduce((sum, device) => sum + Number(device.reading), 0);
          temp_avg = temp_sum / hvac_devices.temp.length;
        } else {
          temp_avg = 0;
        }

        // Calculate humidities
        if (hvac_devices.humidity.length > 0) {
          humidity_sum = hvac_devices.humidity.reduce((sum, device) => sum + Number(device.reading), 0);
          humidity_avg = humidity_sum / hvac_devices.humidity.length;
        } else {
          humidity_avg = 0;
        }

        // Calculate airflow
        if (hvac_devices.air.length > 0) {
          air_sum = hvac_devices.air.reduce((sum, device) => sum + Number(device.reading), 0);
          air_avg = air_sum / hvac_devices.air.length;
        } else {
          air_avg = 0;
        }

        // Pass along HVAC data
        data.hvacs = {
          average_temperature: temp_avg,
          temperatures: hvac_devices.temp.map((device) => {
            return {
              id: device.id,
              hub_id: device.hub_id,
              reading: device.reading,
            };
          }),
          average_humidity: humidity_avg,
          humidities: hvac_devices.humidity.map((device) => {
            return {
              id: device.id,
              hub_id: device.hub_id,
              reading: device.reading
            };
          }),
          average_airflow: air_avg,
          airflows: hvac_devices.air.map((device) => {
            return {
              id: device.id,
              hub_id: device.hub_id,
              reading: device.reading
            };
          }),
        }

        return data;
      });


      if (silos.length < 1) {
        // If no silos 404 and error
        res.status(404).json({ status: 404, message: `No silos by id ${req.params.id}` })
      } else {
        //  Success: send silo(s)
        res.status(200).json({
          status: 200,
          // silos: req.params.id === undefined ? silos : silos[0],
          silos: silo_data,
        });
      }
    })

    // Error
    .catch((error) => this.sendError(res, error));
  }

  getPods(req, res) {
    const silo_id = req.params.id || false;
    Hub.findAll({
      where: { silo_id: silo_id },
      include: [
        {
          model: Crop,
          attributes: [ 'id', 'start_at' ],
          through: { attributes: [] },
          where: { completed_at: null },
          required: false,
          include: {
            model: Recipe,
            attributes: [ 'name' ],
          }
        },
        {
          model: Device,
          include: [
            DeviceType,
            {
              model: Notification,
              required: false,
              where: { status: { $ne: 'done' }},
            },
          ]
        },
      ]
    })

    .then((pods) => {
      // 404 if nothing
      if(pods.length < 1) {
        res.status(404).json({ status: 404, message: 'No pods' });
      } else {
        const data = {
          silo_id,
          status: 200,
        };

        // Restructure pods object
        data.all_pods = pods.map((pod) => {
          const { id, MAC, label, order, silo_id, status } = pod;
          const pod_data = { id, MAC, label, order, silo_id, status };

          // Create array of device types
          pod_data.device_types = pod.devices.reduce((types, device) => {
            // If not in array, then add to array
            if(types.indexOf(device.device_type) === -1) {
              types.push(device.device_type);
            }

            return types;
          }, []);

          pod_data.crop = pod.crops[0];  // Add crop

          // Calculate number of notificaitons
          pod_data.num_notifications = pod.devices.reduce((num_notifications, device) => {
            return num_notifications + device.notifications.length;
          }, 0);

          return pod_data;
        });


        // Calculate pod counts
        data.total_pods = pods.length;
        // SUM pods with a crop
        data.pods_in_use = pods.reduce((acc, value) => value.crops.length > 0 ? acc + 1 : acc, 0);
        data.pods_available = data.total_pods - data.pods_in_use;

        // create array of crops and their pods
        data.crops = data.all_pods.reduce((crops, pod) => {
          if(pod.crop)
          {
            const crop = {
              id: pod.crop.id,
              recipe_name: pod.crop.recipe.name,
              start_at: pod.crop.start_at,
              pods: [],
            }

            // If crop is not yet in crops array
            var index = crops.map((crop) => crop.id).indexOf(crop.id);
            if(index === -1)
            {
              crops.push(crop);
              index = 0;
            }

            // Clone pod and remove crop
            const new_pod = JSON.parse(JSON.stringify(pod));
            delete new_pod.crop;

            // Push to crop array
            crops[index].pods.push(new_pod);
          }

          return crops;
        }, []);

        res.status(200).json(data); // Success: send pods
      }
    })

    // Error
    .catch((error) => this.sendError(res, error));
  }

  getNotifications(req, res) {
    const silo_id = req.params.id || false;

    // Get notifications
    Notification.findAll({
      where: { status: { $ne: 'done' }},
      include: {
        model: Device,        // Join to Device
        include: [
          {
            model: Hub,         // Join to Hub
            where: { silo_id }, // Make sure for only Silo: x
            required: true,
          },
          DeviceType
        ],
      }
    })
    .then((notifications) => {

      res.status(200).json({
        notifications,
      });
    })

    // Error
    .catch((error) => this.sendError(res, error));
  }

  getDevices(req, res) {
    const silo_id = req.params.id || false;
    const hub_id = req.params.hub_id || false;
    const crop_id = req.params.crop_id || false;

    // Create where objects
    const hub_where = {};
    const device_where = {};
    const crop_where = { completed_at: null };
    let crop_required = false;

    // If hub specified only return hub
    if(hub_id !== false)
    {
      device_where.hub_id = hub_id;
    }

    // If silo_id specified return for silo
    if(silo_id !== false)
    {
      hub_where.silo_id = silo_id;
    }

    // If crop_id specified return for crop
    if(crop_id !== false)
    {
      crop_where.id = crop_id;
      crop_required = true;
    }

    // Get all devices
    Device.findAll({
      where: device_where,
      include: [
        {
          model: Hub,
          where: hub_where,
          include: {
            model: Crop,
            through: { attributes: [] },
            attributes: [],
            where: crop_where,
            required: crop_required,
          },
          required: true,  // require silo_id to be present for whole result set
        },
        Notification,
      ],
    })
    .then((devices) => {
      res.status(200).json({
        devices,
      });
    })

    // Error
    .catch((error) => this.sendError(res, error));
  }

  getDevicesByType(req, res) {
    const silo_id = req.params.id || false;

    // Error
    // .catch((error) => this.sendError(res, error));
  }

  sendError(res, error) {
    console.log(error);
    res.status(400).json({
      status: 400,
      error: error.message,
    });
  }
}

export default new SiloController();
