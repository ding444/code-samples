import moment from 'moment';
import { sprintf } from  'sprintf-js';
import {
  Crop,
  Stage,
  LightPhase,
  Recipe,
  SensorParameters,
  WaterSchedule,
  Hub,
  Silo,
  GVResponses,
  Device,
  DeviceType,
  RecipeHVAC,
  HVACType,
} from '../models';
import gv_requests from '../utils/gv_requests';

class CropController {
  constructor() {
    // Bind these methods
    this.get = this.get.bind(this);
    this.start = this.start.bind(this);
    this.harvest = this.harvest.bind(this);
    this.download = this.download.bind(this);
    this.get_available_pods = this.get_available_pods.bind(this);
  }

  /*****************************************
   * List active crops or specific crop with their
   * stage, plant_category and plant_type data
   *****************************************/
  get(req, res) {
    const where = {};
    let crop_id = null;
    let modifier;

    // Parse qualifier into all, archived, or id
    switch(true) {
      // If is numeric
      case (!isNaN(req.params.qualifier)):
        crop_id = req.params.qualifier;
        where.id = req.params.qualifier;
        break;

      // If qualifier is all then show active and archived
      case (req.params.qualifier === 'all'):
        modifier = 'all';
        break;

      // If qualifier is "history" then show active and archived
      case (req.params.qualifier === 'history'):
        where.completed_at = { $ne: null };
        modifier = 'history';
        break;

      // If no req.params.qualifier then show active crops
      case (req.params.qualifier === undefined):
        where.completed_at = null;
        modifier = 'active';
        break;

      // Invalid qualifier sends 404 and stops execution
      default:
        res.status(404).send();
        return;
    }

    // Get crops where completed_at is null (not finished)
    Crop.findAll({
      attributes: { exclude: ['created_at', 'recipe_id'] },
      include: [
        {
          attributes: { exclude: [ 'created_at' ] },
          model: Recipe,
          include: [
            {
              model: Stage,   // Join to stage
              // Join stage to light phases, and water schedules
              include: [
                LightPhase,
                WaterSchedule,
                SensorParameters,  // Join recipe sensor overrides
              ]
            },
            {
              model: SensorParameters,
              /*
               * Only get parameters with no Stage. In other words
               * get recipe parameters as fallback
               */
              where: { stage_id: null },
              required: false,
            },
            {
              model: RecipeHVAC,
              include: HVACType,
            },
          ]
        },
        {
          attributes: {exclude: [ 'silo_id' ]},
          model: Hub,
          through: { attributes: [] },
          include: [
            Silo,
            {
              model: Device,
              where: { device_type_id: 32 },
              required: false,
              include: DeviceType,
            }
          ],
        },
      ],
      where,                    // use where object from above
    })

    // Determine whether watering or not
    .then((crops) => {
      return Promise.all(crops.map((crop) => {
        crop = JSON.parse(JSON.stringify(crop));
        let is_watering = false;

        // get current stage
        const current_stage = crop.recipe.stages.filter((stage) =>
          crop.current_stage_id === stage.id
        )[0];

        // Extract water_schedule duration and rest times
        const { duration: watering_time, rest_period: resting_time } = current_stage.water_schedule;

        // Calculate times
        const now = moment();
        const next_stage_at = moment(crop.next_stage_at);
        const time_remaining_in_stage = Math.floor(next_stage_at.diff(now) / 1000);
        const total_stage_duration = current_stage.duration * current_stage.cycles;
        const time_into_stage = total_stage_duration - time_remaining_in_stage;
        const time_into_stage_cycle = time_into_stage % (current_stage.duration / current_stage.cycles);
        const time_remaining_in_stage_cycle = time_remaining_in_stage % ( watering_time + resting_time );

        // console.info('---');
        // console.info('Stage #', Math.floor(time_into_stage / current_stage.duration) + 1, 'of', current_stage.cycles);
        // console.info('Time into stage:', time_into_stage);
        // console.info('Total time of stage:', total_stage_duration);
        // console.info('Irrigation Cycle #', Math.floor(time_into_stage / ( watering_time + resting_time)), 'of', Math.floor(total_stage_duration / (watering_time + resting_time)));
        // console.info('Time into stage cycle:', time_into_stage_cycle);
        // console.info('Time remaining in stage cycle:', time_remaining_in_stage_cycle);

        // Toggle watering if time is within schedule
        if (time_remaining_in_stage > 0 && time_into_stage_cycle < watering_time) {
          is_watering = true;
        }

        crop.is_watering = is_watering;

        return Promise.resolve(crop);
      }));
    })

    // Success
    .then((crops) => {
      // Output active crops
      const data = {
        code: 200,
        status: 'success',
        message: null,
        error: null,
        data: null,
      };

      if (crop_id !== null && crops.length < 1) {
        // If no crops 404 and error
        data.code = 404;
        data.status = 'error';
        data.message = `No crop by id: ${crop_id}`;
        data.error = {
          type: 'invalid query parameter',
        };
      } else if(crops.length < 1) {
        // If no crops 404 and error
        data.message = 'No crops for query';
        data.data = {
          modifier,
          crops: [],
        };
      } else {
        //  Success: send crop(s)
        data.data = {};
        if (crop_id === null) {
          data.data.modifier = modifier;
          data.data.crops = crops;
        } else {
          data.data.crop = crops[0];
        }
      }

      res.status(data.code).json(data);
    })

    // Error
    .catch((error) => {
      console.error(error);
      res.status(500).json({
        code: 500,
        status: 'error',
        message: error.message,
        error: {
          type: 'general error',
          name: error.name,
        },
        data: null,
      });
    });
  }

  /*****************************************
   * Start (create) a new crop by assigning it a recipe and
   * hubs.  Receives JSON body with recipe_id and array of
   * pods.  Also receives optional starting phase_id
   *****************************************/
  start(req, res) {
    let crop_id;
    const { recipe_id, pods, stage_id, notes } = req.body;

    // Find the recipe to base the crop on
    Recipe.findOne({
      include: [ Stage ],
      // Where recipe_id is what was passed
      where: { id: recipe_id },
    })

    // Create pod from recipe
    .then((recipe) => {
      if(recipe === null) throw Error('No recipe by id');
      // Get stage if specified, otherwise get first stage in recipe
      const stage = recipe.stages.filter((stage) => stage.id === stage_id)[0] || recipe.stages[0];

      // establish start, end, and next_stage times
      const start_date      = moment();
      const end_date        = start_date.clone().add(recipe.total_duration, 'seconds');
      const next_stage_date = start_date.clone().add(stage.duration * stage.cycles, 'seconds');

      // Return promise to next .then()
      return Crop.build({
        recipe_id:             recipe.id,
        //recipe_plant_type:     recipe.plant_type,
        start_at:              start_date,
        finish_at:             end_date,
        current_stage_id:      stage.id,
        notes:                 notes,
        next_stage_at:         next_stage_date,
        // current_phase_id:      ,//TODO get clarification on what this is
      }).save();
    })

    // Create crop_has_hub associations
    .then((crop) => {
      crop_id = crop.id;
      return crop.setHubs(pods)
    })

    // Get Silos Hub
    .then((hub_associations) =>
      Promise.all(hub_associations[0].map((hub) =>
        Hub.findOne({ where: { id: hub.hub_id }})
      ))
    )

    // TODO: Create recipe file
    .then((hubs) => Promise.resolve(hubs))

    // Send download recipe request
    .then((hubs) => {
      return Promise.all(hubs.map((hub) => {
        return gv_requests.download_recipe_file({
          silo: hub.silo_id,
          hub: hub.id,
          device: 0,
          request: 'RECIPE=recipe.txt',
        });
      }));
    })

    // Check for confirmation of recipe download
    .then((requests) => {
      const interval_time = 50;  // 0.1 sec
      const num_attempts = 10;  // Number of times before fail
      let count = 0;  // counter

      // Use only values
      requests = requests.map((request) => {
        const data = request.get();
        data.success = false;
        return data;
      });

      return new Promise((resolve, reject) => {
        // Start interval loop
        const timer = setInterval(() => {
          const promises = [];

          // Get requests that aren't successful
          const unfinished_requests = requests.filter((request) => request.success === false);
          if (unfinished_requests.length < 1) {
            resolve(requests);
          }
          unfinished_requests.forEach((request, index, arr) => {
            // Check for gv_response
            GVResponses.findOne({
              where: { gv_request_id: request.id },
            })
            .then((response) => {
              if (response !== null && response.request_status === 1) {
                arr[index].success = true;
                arr[index].response = response;
              }

              // Push promise to promises
              promises.push(Promise.resolve());
            });
          });

          // Wait for all promises and then continue
          Promise.all(promises)
          .then(() => {
            // If over max attempts stop timer and continue
            if (count++ >= num_attempts) {
              clearInterval(timer);
              requests.forEach((request) => {
                if(request.success === false) {
                  // TODO add some logic here
                  console.info(`No response for request_id: ${request.id}`);
                }
              });
              resolve(requests);
            }
          });

        }, interval_time); // /interval
      }); // /promise

    }) // /then

    //Send recipe_start request -- don't wait for response
    .then((requests) =>
      Promise.all(requests.map((request) => {
        const { silo, hub, device } = request;
        return gv_requests.start_recipe({ silo, hub, device });
      }))
    )

    // Success -- send resposne
    .then((hubs) => {
      // console.log(JSON.stringify(hubs, null, 4));
      res.status(200).json({
        code: 200,
        status: 'success',
        message: 'Crop started',
        error: null,
        data: {
          crop_id,
        },
      });
    })

    // Errors
    .catch((error) => {
      console.error(error.message);
      res.status(500).json({
        code: 500,
        status: 'error',
        message: error.message,
        error: {
          type: 'general error',
          name: error.name,
        },
        data: null,
      });
    });
  }

  /*****************************************
   * Harvest/end crop
   * Record yield, optional notes, and set
   * completion_date
   * TODO validation
   *****************************************/
  harvest(req, res) {
    const {crop_id, crop_yield, crop_notes } = req.body;

    // Get crop
    Crop.findOne({
      where: { id: crop_id },
    })

    //  recipe
    .then((crop) => {
      // If no recipe err
      if(crop === null) throw Error('Invalid crop_id: '+ crop_id);

      // Basic validation

      // Update harvest columns
      crop.yield = crop_yield;
      crop.harvest_notes = crop_notes;
      crop.completed_at = moment();

      return crop.save()  // Save and continue
    })

    // Saved.  Send response
    .then((crop) => {
      res.status(200).json({
        code: 200,
        status: 'success',
        message: 'Crop harvest recorded',
        error: null,
        data: {
          crop_id: crop_id,
        },
      });
    })

    // Catch and handle error
    .catch((error) => {
      console.log(error);
      res.status(500).json({
        code: 500,
        status: 'error',
        message: error.message,
        error: {
          type: 'general error',
          name: error.name,
        },
        data: null,
      });
    })
  }

  /*****************************************
   * Retrieve, format, and offer Recipe for
   * download.
   * TODO: Create better errors handling
   *****************************************/
  download(req, res) {
    let crop_start_stage_index;

    Crop.findOne({
      include: {
        model: Stage,
        as: 'current_stage',
      },
      where: { id: req.params.id }
    })
    .then((crop) => {
      // Store current stage in order to generate recipe file from it
      crop_start_stage_index = crop.current_stage.order;

      // Get crop's recipe
      return Recipe.findOne({
        order: [
          [ Stage, 'order', 'ASC' ] // order by stage order
        ],
        where: { id: crop.recipe_id },
        include: [
          {
            model: Stage,   // Join to stage
            include: [
              LightPhase,
              WaterSchedule,
              SensorParameters,  // Join recipe sensor overrides
            ]
          },
          {
            model: SensorParameters,
            where: { stage_id: null },
            required: false,
          },
          {
            model: RecipeHVAC,
            include: HVACType,
          },
        ],
      })
    })

    // filter to correct stage
    .then((recipe) => {
      recipe.stages = recipe.stages.filter((item) => {
        return item.order >= crop_start_stage_index;
      });

      // return modified recipe
      return Promise.resolve(recipe);
    })

    // Present file for download
    .then((recipe) => {
      // Set header for download
      res.setHeader('Content-disposition', 'attachment; filename=recipe.txt');
      res.setHeader('Content-type', 'text/plain');

      // Format recipe and pass querystring parameters as options
      const formatted_recipe = this.format_recipe_file(recipe, req.query);

      // Send recipe
      res.send(formatted_recipe);
    })
    .catch((err) => {
      console.log(err);
      res.status(400).json({status: 400, error: err.message});
    });
  }

  // This may make more sense to move to a utilities directory
  // and/or broken up to be re-used elsewhere
  format_recipe_file(recipe, options) {
    // Class for adding tabs and new line characters readable
    class Output {
      constructor() {
        this.tab_spaces = '\t';  // Tab character(s)
        this.output = '';
      }
      // adds new tabs, string, and new line to output
      add(tabs = 0,  str = '') {
        this.output += this.tab_spaces.repeat(tabs) + str +'\n';
      }
      // Removes last line from output
      rm_line() { this.output = this.output.split('\n').slice(0,-1).join('\n') }
      get() { return this.output; } // Return output string
    }

    const output = new Output();

    // Create header and HVAC
    output.add(0, 'RECIPE');
    output.add(0, '{');
    output.add(1, 'HVAC');
    output.add(1, '{');

    // TODO: Create HVAC
    const temp     = recipe.recipe_hvacs.filter((item) => item.hvac_type_id === 1)[0];
    const humidity = recipe.recipe_hvacs.filter((item) => item.hvac_type_id === 2)[0];
    const air      = recipe.recipe_hvacs.filter((item) => item.hvac_type_id === 3)[0];

    output.add(2, sprintf('%-10s %-3s %-3s','TEMP', temp.min, temp.max));
    output.add(2, sprintf('%-10s %-3s %-3s','HUMIDITY', humidity.min, humidity.max));
    output.add(2, sprintf('%-10s %-3s %-3s','AIR', air.min, air.max));

    output.add(1, '}');  // Close HVAC
    output.add();

    // Create stages
    recipe.stages.forEach((stage) => {
      output.add(1, '*'+ stage.name);
      output.add(1, 'CYCLES '+ stage.cycles);
      output.add(1, '{');
      output.add(2, 'LIGHT-PHASE');
      output.add(2, '{');

      // Loop through light phases
      stage.light_phases.forEach((light_phase) => {
        // use same formatting for red, blue, warm, and cool
        [ 'red', 'blue', 'warm', 'cool' ].forEach((color) => {
          output.add(3, sprintf('%-10s %s', color.toUpperCase(), light_phase[color]));
        });

        output.add(3, sprintf('%-10s %s', 'DURATION', light_phase.duration));  // Duration
        output.add();  // Add new line in between each item of light_phases array
      });

      output.rm_line();   // Delete last empty line
      output.add(2, '}'); // Close LIGHT-PHASE

      // Water phases
      output.add(2, 'WATER-PHASE');
      output.add(2, '{');

      // Create water schedules
      // stage.water_schedules.forEach((water_schedule) => {
        output.add(3, sprintf('%-10s %s', 'ON', stage.water_schedule.duration));
        output.add(3, sprintf('%-10s %s', 'OFF', stage.water_schedule.rest_period));

        // Nutrient dosing
        const {nutrient_a,nutrient_b,nutrient_c,nutrient_d} = stage.water_schedule;
        output.add(3, sprintf('%-10s %-3s %-3s %-3s %-3s', 'DOSE', nutrient_a,nutrient_b,nutrient_c,nutrient_d));
        output.add();     // Add new line in between each item of water_schedules array
      // });

      output.rm_line();   // Delete trailing new line
      output.add(2, '}'); // Close WATER-PHASE
      output.add(1, '}'); // Close  CYCLE

    });

    output.add(0, '}');   // Close RECIPE

    return output.get();
  }

  /*****************************************
   * Get a list of silos with their availble pods
   *****************************************/
  get_available_pods(req, res) {
    Silo.findAll({
      include: {
        model: Hub,
        include: {
          model: Crop,
          through: { attributes: [] },
          attributes: [ 'completed_at' ],
          where: { completed_at: null },
          required: false,
        },
      }
    })

    // Filter for empty crops
    .then((silos) => {
      // Clone silos
      const new_silos = JSON.parse(JSON.stringify(silos));

      // iterate through each silo
      const return_data = new_silos.map((silo) => {
        // Check for hubs w/out crops
        silo.available_hubs = silo.hubs.filter((hub) => hub.crops.length < 1);

        delete silo.hubs; // Remove hus property
        return silo;
      });

      return Promise.resolve(return_data);
    })

    // Ship it
    .then((silos) => {
      res.status(200).json({
        code: 200,
        status: 'success',
        message: null,
        error: null,
        data: {
          silos: silos,
        },
      });
    })

    // Catch and handle error
    .catch((error) => {
      console.log(error);
      res.status(500).json({
        code: 500,
        status: 'error',
        message: error.message,
        error: {
          type: 'general error',
          name: error.name,
        },
        data: null,
      });
    })
  }
}

// Instantiate and export object
export default new CropController();
