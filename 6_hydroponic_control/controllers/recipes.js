import { Validator } from 'jsonschema';
import {
  Recipe,
  Stage,
  LightPhase,
  WaterSchedule,
  SensorParameters,
  RecipeHVAC,
  HVACType,
} from '../models';

class RecipeController {
  /*****************************************
   *
   *****************************************/
  constructor() {
    /*
     * Binding these methods to this class's instance to
     * avoiding having to bind methods in routes/recipes.js
     * e.g. recipe.get('/', RecipeController.get.bind(RecipeController)
     */
    this.get = this.get.bind(this);
    this.create = this.create.bind(this);
    this.delete = this.delete.bind(this);

    // Initialize jsonschema validator
    this.validator = new Validator();

    // Shared include for retrieval
    this.recipe_include = [
        {
          model: Stage,   // Join to stage
          // Join stage to light phases, and water schedules
          include: [
            LightPhase,
            WaterSchedule,
            SensorParameters,  // Join recipe sensor overrides
          ]
        },
        {
          model: SensorParameters,
          /*
           * Only get parameters with no Stage. In other words
           * get recipe parameters as fallback
           */
          where: { stage_id: null },
          required: false,
        },
        {
          model: RecipeHVAC,
          include: HVACType,
        },
      ];
  }


  /*****************************************
   * Get single record by ID if req.params.id
   * otherwise get all records.  Results are
   * joined to stages which are joined to light
   * phases, and water schedules
   *****************************************/
  get(req, res) {
    const where = {};
    let recipe_id = null;
    let modifier;

    // Parse qualifier into all, archived, or id
    switch(true) {
      // If is numeric
      case (!isNaN(req.params.qualifier)):
        recipe_id = req.params.qualifier;
        where.id = req.params.qualifier;
        break;

      // If qualifier is all then show active and archived
      case (req.params.qualifier === 'all'):
        modifier = 'all';
        break;

      // If qualifier is "history" then show active and archived
      case (req.params.qualifier === 'history'):
        where.status = { $ne: 'active' };
        modifier = 'history';
        break;

      // If no req.params.qualifier then show active crops
      case (req.params.qualifier === undefined):
        where.status = 'active';
        modifier = 'active';
        break;

      // Invalid qualifier sends 404 and stops execution
      default:
        res.status(404).send();
        return;
    }

    // Get recipe
    Recipe.findAll({
      attributes: {exclude: ['created_at']},
      where: where,
      include: this.recipe_include,
    })

    // Resume with promise
    .then(recipes => {
      // If invalid recipe return 404
      if (recipe_id && recipes.length === 0) {
        res.status(404).json({
          code: 404,
          status: 'error',
          message: 'Recipe not found',
          error: {
            type: 'invalid query parameter'
          },
          data: null,
        });
      } else {
        // Success
        const data = {
          code: 200,
          status: 'success',
          message: null,
          error: null,
          data: {},
        };

        if (recipe_id !== null) {
          data.data.recipe = recipes[0];
        } else {
          data.data.modifier = modifier;
          data.data.recipes = recipes;
        }

        res.status(200).json(data);
      }
    })
  // Handle error and output
    .catch(error => {
      console.log(error);
      res.status(500).json({
        code: 500,
        status: 'error',
        message: error.message,
        error: {
          type: 'server error',
        },
        data: null,
      })
    });
  }


  /*****************************************
   * Create new or update existing model by
   * validating json object and inserting into DB
   *****************************************/
  create(req, res) {
    const recipe = req.body;
    let new_recipe;
    const modify = (req.params.id !== null && req.method == 'PUT') ? true : false;

    // Calculate duration
    recipe.total_duration = recipe.stages.reduce((acc, stage) =>
      acc + stage.duration * stage.cycles
    , 0);

    // Validate recipe against JSON schemas
    const promise = this.validate_recipe(recipe);

    promise
      .then((recipe) => {
        // If PUT method and ID is set then update
        // by cloning and adding derived_id
        // instead of create a new one
        if(modify === true) {
          recipe.derived_id = Number(req.params.id);   // set derived id to old id
          delete recipe.id;                            // delete id
        }

        // Build new recipe using Recipe model
        new_recipe = Recipe.build(recipe, { include: this.recipe_include });

        // Save new recipe to db
        return new_recipe.save();

      })

      // Recipe saved, success
      .then((new_recipe) => {
        res.status(200).json({
          code: 200,
          status: 'success',
          message: `recipe ${modify === true ? 'updated' : 'created'}`,
          error: null,
          data: {
            recipe_id: new_recipe.id,
            derived_id: new_recipe.derived_id,
            recipe: new_recipe,
          },
        });
      })

      // If errors
      .catch((error) => {
        console.log(error);

        // Send invalid request and errors
        res.status(500).json({
          code: 500,
          status: 'error',
          message: error.message,
          error: {
            type: 'server error',
          },
          data: null,
        });
      });
  }


  /*****************************************
   * Delete recipe
   * Actually toggles a status flag
   *****************************************/
  delete(req, res) {
    // Get recipe by ID
    Recipe.findOne({
      where: { id: req.params.id }
    })
      .then(recipe => {
        // If invalid recipe return 404
        if(recipe == null) {
          res.status(404).json({
            code: 404,
            status: 'error',
            message: 'Invalid recipe',
            error: {
              type: 'invalid query parameter'
            },
            data: null,
          });
        }
        else {
          // Success
          recipe.status = 'archived';
          recipe.save();

          res.status(200).json({
            code: 200,
            status: 'success',
            message: 'recipe deleted',
            error: null,
            data: {
              recipe_id: recipe.id,
            },
          });
        }
      })
      .catch((error) => res.status(500).json({
        code: 500,
        status: 'error',
        message: error.message,
        error: {
          type: 'server error',
        },
        data: null,
      }));
    }

  /*****************************************
   * Validate recipe JSON using JSON Schema
   * http://json-schema.org/ for reference
   *****************************************/

  // TODO: Add finer validation

  validate_recipe(recipe) {
    // JSON Schema for recipe
    const recipe_schema = {
      id: '/recipe_schema',
      type: 'object',
      properties: {
        name: { type: 'string' },
        derived_id: { type: [ 'integer', 'null' ] },
        plant_type: { type: 'string' },
        stages: {
          type: 'array',
          items: { $ref: '/stage_schema' },
          minItems: 1,
        },
        sensor_parameters: {
          type: 'array',
          items: { $ref: '/sensor_parameters_schema' },
        },
        recipe_hvacs: {
          type: 'array',
          items: { $ref: '/recipe_hvac_schema' },
        }
      },
      required: [ 'name', 'plant_type', 'stages' ],
    };

    // JSON Schema for stage
    const stage_schema = {
      id: '/stage_schema',
      type: 'object',
      properties: {
        name: { type: 'string' },
        order: { type: 'integer' },
        cycles: { type: 'integer' },
        light_phases: {
          type: 'array',
          items: { $ref: '/light_phase_schema' },
          minItems: 1,
        },
        water_schedule: { $ref: '/water_schedule_schema' },
        sensor_parameters: {
          type: 'array',
          items: '/sensor_parameters_schema',
        },
      },
      required: [
        'name',
        'order',
        'cycles',
        'light_phases',
        'water_schedule',
      ],
    }

    // JSON Schema for light phases
    const light_phase_schema = {
      id: '/light_phase_schema',
      type: 'object',
      properties: {
        order: { type: 'integer' },
        duration: { type: 'integer' },
        warm: { type: 'integer' },
        cool: { type: 'integer' },
        red: { type: 'integer' },
        blue: { type: 'integer' },
      },
      required: [ 'order', 'duration', 'warm', 'cool', 'red', 'blue'  ],
    }

    // JSON Schema for water schedule
    const water_schedule_schema = {
      id: '/water_schedule_schema',
      type: 'object',
      properties: {
        duration: { type: 'integer' },
        rest_period: { type: 'integer' },
        nutrient_a: { type: 'integer' },
        nutrient_b: { type: 'integer' },
        nutrient_c: { type: 'integer' },
				nutrient_d: { type: 'integer' },
        ph_min: { type: 'integer'},
        ph_max: { type: 'integer'},
      },
      required: [
        'duration',
        'rest_period',
        'nutrient_a',
        'nutrient_b',
        'nutrient_c',
        'nutrient_d',
        'ph_min',
        'ph_max',
      ],
    }

    const sensor_parameters_schema = {
      id: '/sensor_parameters_schema',
      type: 'object',
      properties: {
        sensor_type: { type: 'integer' },
        sensor_max: { type: 'integer' },
        sensor_min: { type: 'integer' },
      },
      require: [ 'sensor_type', 'sensor_max', 'sensor_min' ],
    }

    const recipe_hvac_schema = {
      id: '/recipe_hvac_schema',
      type: 'object',
      properties: {
        hvac_type_id: { type: 'integer' },
        max: { type: 'integer' },
        min: { type: 'integer' },
      },
      require: [ 'hvac_type_id', 'max', 'min' ],
    }

    // Load schemas into validator
    this.validator.addSchema(recipe_schema, '/recipe_schema');
    this.validator.addSchema(stage_schema, '/stage_schema');
    this.validator.addSchema(light_phase_schema, '/light_phase_schema');
    this.validator.addSchema(water_schedule_schema, '/water_schedule_schema');
    this.validator.addSchema(sensor_parameters_schema, '/sensor_parameters_schema');
    this.validator.addSchema(recipe_hvac_schema, '/recipe_hvac_schema');

    // validate
    return new Promise((resolve, reject) => {
      // run validator
      const result = this.validator.validate(recipe, recipe_schema);

      // check for errors
      if(result.errors.length > 0) {
        const error = new Error('Validation error');
        error.data = result['errors'];   // append errors to Error object
        reject(error);
      } else {
        resolve(recipe);  // successfull validation
      }
    });
  }


}

export default new RecipeController();
