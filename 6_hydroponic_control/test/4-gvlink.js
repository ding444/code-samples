import chai from 'chai';
import chaiHttp from 'chai-http';
import chaiThings from 'chai-things';
import app from '../index';

const expect = chai.expect

chai.use(chaiHttp);
chai.use(chaiThings);

chai.should();

/*****************************************
 * Reusable function to test multiple endpoints
 *****************************************/
function make_generic_request(method, name, data = {}) {
  return new Promise((resolve, reject) => {
    // Mini function for handling chai .end()
    function handle_response(error, res) {
      if (error) throw error;
      resolve(res);
    }

    if (method === 'post') {
      chai.request(app)
        .post(`/api/gvlink/${name}/silo/0/hub/1/device/1`)
        .send(data)
        .end(handle_response);
    } else {
      chai.request(app)
        .post(`/api/gvlink/${name}/silo/0/hub/1/device/1`)
        .send(data)
        .end(handle_response);
    }
  });
}

/*****************************************
 * Reusable function to validate multiple
 * endpoints
 *****************************************/
function validate_generic_request(res) {
  expect(res.status).to.equal(200);
  expect(res.body.data.request.read_at).to.not.be.null;
  expect(res.body.data.request.processed_at).to.not.be.null;
  expect(res.body.data.response.response).to.equal('OK');

  return Promise.resolve(res);
}


/*****************************************
 * GV Communications endpoint tests
 *****************************************/
describe('GV Communications Endpoints', function() {
  // override default timeout to make it faster if GVLink is not running
  this.timeout(300); 

  describe('`get tree`', function (done) {
    this.timeout(5000);  // Override default timeout for longer requests
    it('should create a gv_request and wait for a gv_response', function(done) {
      chai.request(app)
        .get('/api/gvlink/get_tree')
        .end((error, res) => {
          expect(res.status).to.equal(200);
          expect(res.body.data.request.read_at).to.not.be.null;
          expect(res.body.data.request.processed_at).to.not.be.null;
          expect(res.body.data.response.response).to.equal('OK');
          done();
        });
    });
  });

  // TODO: I will work on these later, but they don't do much right now in GVLink

  // describe('`get system time`', () => {
  //   it('should log the request to the db');
  //   it('should have a response in the db');
  // });

  // describe('`set system time`', () => {
  //   it('should log the request to the db');
  //   it('should have a response in the db');
  // });

  // describe('`post system time`', () => {
  //   it('should log the request to the db');
  //   it('should have a response in the db');
  // });

  describe('`post wand intensity`', () => {
    it('should create a gv_request and wait for a gv_response', (done) => {
      make_generic_request('post', 'wand_intensity', {
        red: 87,
        blue: 99,
        warm: 66,
        cool: 44,
      })
      .then(validate_generic_request)
      .then((res) => done())
      .catch(done);
    });
  });

  describe('`get wand intensity`', () => {
    it('should create a gv_request and wait for a gv_response', (done) => {
      make_generic_request('get', 'wand_intensity')
      .then(validate_generic_request)
      .then((res) => done())
      .catch(done);
    });
  });

  describe('`post pump data`', () => {
    it('should create a gv_request and wait for a gv_response', (done) => {
      make_generic_request('post', 'pump_data', {
        pump: 'on',
        maxp: 32,
        minp: 3,
        maxf: 88,
        minf: 66,
      })
      .then(validate_generic_request)
      .then((res) => done())
      .catch(done);
    });
  });

  describe('`get pump status`', () => {
    it('should create a gv_request and wait for a gv_response', (done) => {
      make_generic_request('get', 'pump_data')
      .then(validate_generic_request)
      .then((res) => done())
      .catch(done);
    });
  });

  describe('`post valve data`', () => {
    it('should create a gv_request and wait for a gv_response', (done) => {
      make_generic_request('post', 'valve_data', {
        valve: 'close',
        maxf: 99,
        minf: 87,
      })
      .then(validate_generic_request)
      .then((res) => done())
      .catch(done);
    });
  });

  describe('`get valve status`', () => {
    it('should create a gv_request and wait for a gv_response', (done) => {
      make_generic_request('get', 'valve_data')
      .then(validate_generic_request)
      .then((res) => done())
      .catch(done);
    });
  });

  describe('`post dosing data`', () => {
    it('should create a gv_request and wait for a gv_response', (done) => {
      make_generic_request('post', 'dosing_data', {
        pump1: 100,
        pump2: 87,
        pump3: 7,
        pump4: 76,
      })
      .then(validate_generic_request)
      .then((res) => done())
      .catch(done);
    });
  });

  describe('`get dosing status`', () => {
    it('should create a gv_request and wait for a gv_response', (done) => {
      make_generic_request('get', 'dosing_data')
      .then(validate_generic_request)
      .then((res) => done())
      .catch(done);
    });
  });

  describe('`post hvac data`', () => {
    it('should create a gv_request and wait for a gv_response', (done) => {
      make_generic_request('post', 'hvac_data', {
        maxt: 73,
        mint: 52,
      })
      .then(validate_generic_request)
      .then((res) => done())
      .catch(done);
    });
  });

  describe('`get hvac status`', () => {
    it('should create a gv_request and wait for a gv_response', (done) => {
      make_generic_request('get', 'hvac_data')
      .then(validate_generic_request)
      .then((res) => done())
      .catch(done);
    });
  });

  describe('`post humidity data`', () => {
    it('should create a gv_request and wait for a gv_response', (done) => {
      make_generic_request('post', 'humidity_data', {
        maxh: 39,
        minh: 22,
      })
      .then(validate_generic_request)
      .then((res) => done())
      .catch(done);
    });
  });

  describe('`get humidity status`', () => {
    it('should create a gv_request and wait for a gv_response', (done) => {
      make_generic_request('get', 'humidity_data')
      .then(validate_generic_request)
      .then((res) => done())
      .catch(done);
    });
  });


  // TODO: I will do something with these later.  Right now there is some confusion and ambiguity as to how these work in the GVLink test

  // describe('`download recipe file`', () => {
  // });

  // describe('`start recipe`', () => {
  // });

  // describe('`stop recipe`', () => {
  // });

  it('should be awesome');
})
