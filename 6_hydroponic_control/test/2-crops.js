import chai from 'chai';
import chaiHttp from 'chai-http';
import chaiThings from 'chai-things';
import app from '../index';
import new_recipe_json from '../_data/test_recipe';

const expect = chai.expect

chai.use(chaiHttp);
chai.use(chaiThings);

chai.should();


/*****************************************
 * Recipe endpoints Unit tests
 * Checks:
 *   GET    /api/crops
 *   GET    /api/crops/1
 *   GET    /api/crops/1/download
 *   GET    /api/crops/pods
 *   POST   /api/crops
 *   POST   /api/crops/1/harvest
 *
 *****************************************/

describe('Crop API Endpoints Test', function() {
  let pods, recipe_id, crop_id, pod_ids;

  before((done) => {
    chai.request(app)
      .post('/api/recipes')
      .send(new_recipe_json)
      .then((res) => {
        recipe_id = res.body.data.recipe.id;
        done();
      })
      .catch((error) => {
        console.log(error.message);
        done();
      })

  });

  it('should find available pods', (done) => {
    chai.request(app)
      .get('/api/crop/pods')
      .end((error, res) => {
        res.status.should.equal(200);
        const silos = res.body.data.silos;

        silos.should.be.an('array');
        silos.should.all.have.property('available_hubs');

        pods = silos.reduce((acc, silo) => {
          acc = acc.concat(silo.available_hubs);
          return acc;
        }, []);

        done();
      })
  });

  it('should create a crop from a recipe and available pods', function(done) {
    this.timeout(8000);

    pod_ids = pods.filter((pod) => pod.silo_id === pods[0].silo_id)
      .map((pod) => pod.id)
      .slice(0, 2);

    chai.request(app)
      .post('/api/crops')
      .send({
        recipe_id,
        pods: pod_ids,
        notes: 'Obligatory note test',
      })
      .end((error, res) => {
        if(error) throw error;

        res.status.should.equal(200);
        crop_id = res.body.data.crop_id;

        done();
      });
  });

  it('should be available by id', (done) => {
    chai.request(app)
      .get(`/api/crops/${crop_id}`)
      .end((error, res) => {
        expect(res.status).to.equal(200);
        expect(res.body.data.crop.id).to.equal(crop_id);
        expect(res.body.data.crop.harvested_at).to.be.null;
        expect(res.body.data.crop.completed_at).to.be.null;
        expect(res.body.data.crop.hubs.map((hub) => hub.id)).to.have.same.members(pod_ids);
        expect(res.body.data.crop.recipe.id).to.equal(recipe_id);

        done();
      });
  });

  it('should make those pods unavailable', (done) => {
    chai.request(app)
      .get('/api/crop/pods')
      .end((error, res) => {
        res.status.should.equal(200);
        const silos = res.body.data.silos;

        silos.should.be.an('array');
        silos.should.all.have.property('available_hubs');

        pods = silos.reduce((acc, silo) => {
          acc = acc.concat(silo.available_hubs);
          return acc;
        }, []);

        const pod_id_set = pods.map((pod) => pod.id );
        expect(pod_ids).to.not.include.members(pod_id_set);

        done();
      })
  });

  it('should harvest and update crop', (done) => {
    chai.request(app)
      .post('/api/crops/harvest')
      .send({
        crop_id,
        crop_yield: 195,
        crop_notes: 'obligatory harvest notes',
      })
      .end((error, res) => {
        if (error) throw error;

        expect(res.status).to.equal(200);
        expect(res.body.data.crop_id).to.equal(crop_id);
        expect(res.body.message).to.equal('Crop harvest recorded');
        done();
      })
  });

  it('should update crop after harvest', (done) => {
    chai.request(app)
      .get(`/api/crops/${crop_id}`)
      .end((error, res) => {
        if (error) throw error;

        expect(res.status).to.equal(200);
        expect(res.body.data.crop.id).to.equal(crop_id);
        expect(res.body.data.crop.completed_at).to.not.be.null;
        expect(res.body.data.crop.yield).to.not.be.null;
        expect(res.body.data.crop.harvest_notes).to.not.be.null;

        done()
      });
  });

  it('should free up pods after harvest');

  it('should create a recipe file for gvlink');
});

