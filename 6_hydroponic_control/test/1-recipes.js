import chai from 'chai';
import chaiHttp from 'chai-http';
import chaiThings from 'chai-things';
import app from '../index';
import new_recipe_json from '../_data/test_recipe';

chai.use(chaiHttp);
chai.use(chaiThings);

chai.should();

// Validate function to validate recipe
const validate_recipe = (id, callback) => {
  chai.request(app)
    .get(`/api/recipes/${id}`)
    .end((err, res) => {
      const recipe = res.body.data.recipe;

      // Validate successfull and JSON
      res.should.have.status(200)
      res.should.be.json;

      // Validate recipe table data
      recipe.should.be.an('object');
      recipe.should.have.property('id', id);
      recipe.should.have.property('stages');
      recipe.stages.should.be.an('array');
      recipe.stages.should.have.length.above(0);

      // Validate stages
      recipe.stages.forEach((stage) => {
        stage.recipe_id.should.equal(recipe.id);

        // Check light_phase for array.length > 0 and proper stage_id
        stage.should.have.a.property('light_phases');
        stage.light_phases.should.be.an('array');
        stage.light_phases.should.have.length.above(0);
        stage.light_phases.should.all.contain.an.item.with.property('stage_id', stage.id);

        // Check water_schedules for array.length > 0 and proper stage_id
        stage.should.have.a.property('water_schedule');
        stage.water_schedule.should.be.an('object');
        stage.water_schedule.should.include.keys(
          'duration',
          'rest_period',
          'nutrient_a',
          'nutrient_b',
          'nutrient_c',
          'nutrient_d',
          'ph_max',
          'ph_min',
        );
        stage.water_schedule.should.have.a.property('stage_id', stage.id);
      });

      // finish with callback
      callback();
    });
};


/*****************************************
 * Recipe endpoints Unit tests
 * Checks:
 *   GET    /api/recipes
 *   GET    /api/recipes/1
 *   POST   /api/recipes
 *   PUT    /api/recipes/1
 *   DELETE /api/recipes/1
 *
 * TODO: Need to finish DELETE validation
 *****************************************/
describe('Recipe endpoints', () => {
  /*****************************************
   * Gets list of all recipes
   *****************************************/
  describe('GET /api/recipes', () => {
    let recipe_ids;

    before((done) => {
      let promises = [];
      for(let i=0,l=5; i<l; i++) {
        const promise = chai.request(app)
          .post('/api/recipes')
          .send(new_recipe_json);

        promises.push(promise);
      }

      Promise.all(promises)
        .then((responses) => {
          recipe_ids = responses.map((res) => res.body.data.recipe_id);
          done();
        })
    });

    it('should list all recipes', (done) => {
      chai.request(app)
        .get('/api/recipes')
        .end((err, res) => {
          const recipes = res.body;

          res.should.have.status(200);  // Check 200 success
          res.should.be.json;           // Should be JSON
          recipes.data.recipes.should.be.an('array'); // Should return array

          const test = recipes.data.recipes.map((recipe) => recipe.id).filter((id) => recipe_ids.includes(id));
          recipe_ids.should.have.lengthOf(test.length);

          // finish with callback
          done();
        });
    });
  });


  /*****************************************
   * Retrives and validates single recipe
   *****************************************/
  describe('GET /api/recipes/1', () => {
    let recipe_id;

    before((done) => {
      chai.request(app)
        .post('/api/recipes')
        .send(new_recipe_json)

        .then((res) => {
          recipe_id = res.body.data.recipe_id;
          done();
        });
    });

    it('should return a recipe with id: '+ recipe_id, (done) => {
      validate_recipe(recipe_id, done)
    });
  });


  /*****************************************
   * Adds and validates new recipe
   *****************************************/
  describe('POST /api/recipes', () => {
    let new_id; // for id after creation

    it('should create a new recipe with associated stages, light_phases, water_schedules, and nutrient_dosing that validates', (done) => {
      chai.request(app)
        .post('/api/recipes')
        .send(new_recipe_json)
        .end((err, res) => {
          const data = res.body.data;

          // Check whether successful
          res.should.have.status(200);
          res.should.be.json;

          // Check that ID returned
          data.recipe_id.should.not.be.empty;
          new_id = data.recipe_id || false;

          // Validate recipe structure of newly inserted data
          validate_recipe(new_id, done);
        });
    });

    it('recipe should be accessible by its id and pass validation', (done) => {
      validate_recipe(new_id, done)
    });

  });


  /*****************************************
   * Creates and verifies child recipe
   *****************************************/
  describe('PUT /api/recipes/1', () => {
    let recipe_id; // for id after creation
    let derived_id;

    before((done) => {
      chai.request(app)
        .post('/api/recipes')
        .send(new_recipe_json)

        .then((res) => {
          recipe_id = res.body.data.recipe_id;
          done();
        });
    });


    it('should have a parent recipe that is available', (done) => {
      validate_recipe(recipe_id, done);
    });

    it('should create a new child recipe with associated stages, light_phases, water_schedules, and nutrient_dosing that validates', (done) => {

      // Clone test recipe data and change id for parent
      let json_data = JSON.parse(JSON.stringify(new_recipe_json));
      // json_data.id = recipe_id;

      chai.request(app)
        .put('/api/recipes/'+ recipe_id)
        .send(json_data)
        .end((err, res) => {
          const data = res.body.data;

          res.should.have.status(200);
          res.should.be.json;

          data.recipe_id.should.not.be.empty;
          derived_id = data.derived_id || false;

          // Validate recipe structure of newly inserted data
          validate_recipe(recipe_id, done);
        });
    });

    it(`should match its parent recipe's id`, () => {
      derived_id.should.equal(recipe_id);
    });
  });


  /*****************************************
   * Deletes recipe and verifies that it
   * is not accessible
   *****************************************/
  describe('DELETE /api/recipes/1', () => {
    let recipe_id;

    before((done) => {
      chai.request(app)
        .post('/api/recipes')
        .send(new_recipe_json)

        .then((res) => {
          recipe_id = res.body.data.recipe_id;
          done();
        });
    });

    it('should make a recipe inaccessible by id', (done) => {
      chai.request(app)
        .delete('/api/recipes/'+ recipe_id)
        .end((err, res) => {
          res.should.have.status(200);
          res.should.be.json;

          res.body.message.should.equal('recipe deleted');

          done();
        });
    });

    it('recipe should be unavailable after deletion', (done) => {
      chai.request(app)
        .get('/api/recipes/get/'+ recipe_id)
        .end((res) => {
          res.status.should.equal(404);
          done();
        });
    });;
  });

});
