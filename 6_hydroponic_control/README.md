# Hydroponics control platform

## Introduction

This is a project I have been working on for the last two months that is a backend control system for a large-scale hydropnoic farm operation.  This is an Express framework backend built on Node.js that interfaces with a React front-end.  It manages a network of Raspberry Pi controllers attached to sensor and device arrays including RGB lighting, HVAC control, irrigation control, and nutrient dosing.

This backend coupled with the front-end controls the entire operation between planting and harvest of the crops.  It handles the crops with growing "recipes" and monitors the environment and hardware notifying the appropriate individuals of potential sensor or device failures and when the crops are completed.

## Why I think it's cool

This project uses many fun pieces of technologies that I have spent years learning.  We're using cuting edge web technologies with ES6 syntax, Linux, Raspberry Pis, and networking.  All things I love to work with.  On top of that it uses some of my agricultural experience.  It also has a well-designed and attractive looking UI.  There are also unit tests that check the integrity of things as I make updates
