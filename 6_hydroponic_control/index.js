import express from 'express';
import helmet from 'helmet';
import { createServer } from 'http';
import { SERVER_PORT } from './utils/env';

import api from './routes';

// Initialize express, http server, and set port
const app = express();
const server = createServer(app);
const port = SERVER_PORT || 3001;

// Express settings and middleware
app.use(helmet());           // Security package
app.use(helmet.noCache());   // Disable client-side caching

// Api routes
app.use('/api', api);

// Start server
server.listen(port, function (){
	console.log(`API server listening on port ${port}`);
});

export default server;
