/*****************************************
 * This is a sequelize instance utility
 * for passing the connectinoa round models
 *****************************************/
import Sequelize from 'sequelize';
import { MYSQL_USER, MYSQL_PASS, MYSQL_DB, MYSQL_HOST } from '../utils/env';

// This config needs to generated from .env settings
const config = {
  host: MYSQL_HOST || 'localhost',
  username: MYSQL_USER || 'root',
  password: MYSQL_PASS || '',
  database: MYSQL_DB || 'gv_communication',
  logging: false
}

// Initialize and return sequelize
const sequelize = new Sequelize(
  config.database, config.username, config.password, config
);

export default sequelize;
