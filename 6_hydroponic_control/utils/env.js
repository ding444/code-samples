import dotenv from 'dotenv';
import fs from 'fs';

// If USE_TEST then use /.env.test for env vars
if(process.env.USE_TEST === 'true') {
  dotenv.config({
    path: process.cwd() + '/.env.test',
  });
} else if(fs.existsSync(process.cwd() + '/.env')) {
  // otherwise use /.env for env vars
  dotenv.config();
}

// make available these env vars
export const {
  PORT,
  DONT_BUILD_KNEX,
  SQL_URL,
  INTERFACE,
  INTERFACE_URL,
  NODE_ENV,
  WATCH_BUILD,
  REDIS_URL,
  MYSQL_HOST,
  MYSQL_USER,
  MYSQL_PASS,
  MYSQL_DB,
  SERVER_PORT,
} = process.env;

export const TIME_PROPORTION = parseFloat(process.env.TIME_PROPORTION) || 1;
