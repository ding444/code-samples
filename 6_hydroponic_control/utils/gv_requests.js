import { GVRequests } from '../models';

/*
 * Object that aids in the use of gv_statuses
 *
 * Reference table
 * | Cod | Description          | Sil | Hub | Dev | Request
 * | --: | :------------------- | :-: | :-: | :-: | :-----------------------------------------------
 * |   1 | Get Tree             |     |     |     | (NO COMMANDS) Rebuilds SQL
 * |   2 | Set System Time      |     |     |     | DATE=1/1/2017, TIME=12:12:12
 * |   3 | Post System Time     |     |     |     | (NO COMMANDS) POST IT
 * |   4 | Get System Time      |  ✓  |  ✓  |  ✓  | DATE=1/1/2017, TIME=12:12:12
 * |   5 | Post Wand Intensity  |  ✓  |  ✓  |  ✓  | RED=100, BLUE=100, WARM=100, COOL=100
 * |   6 | Get Wand Intensity   |  ✓  |  ✓  |  ✓  | RED=100, BLUE=100, WARM=100, COOL=100, TEMP=35
 * |   7 | Post Pump Data       |  ✓  |  ✓  |  ✓  | PUMP=ON/OFF, MAXP=35, MINP=10, MAXF=100, MINF=
 * |   8 | Get Pump Data        |  ✓  |  ✓  |  ✓  | PUMP=ON/OFF, PRESS=35, FLOW=10
 * |   9 | Post Valve Data      |  ✓  |  ✓  |  ✓  | VALVE=OPEN/CLOSE, MAXF=100, MINF=10
 * |  10 | Get Valve Data       |  ✓  |  ✓  |  ✓  | VALVE=OPEN/CLOSE, FLOW=80
 * |  11 | Post Dosing Data     |  ✓  |  ✓  |  ✓  | PUMP1=100, PUMP2=100, PUMP3=100, PUMP4=100
 * |  12 | Get Dosing Data      |  ✓  |  ✓  |  ✓  | PUMP1=100, PUMP2=100, PUMP3=100, PUMP4=100
 * |  13 | Post HVAC Data       |  ✓  |  ✓  |  ✓  | MAXT=75, MINT=68
 * |  14 | Get HVAC Data        |  ✓  |  ✓  |  ✓  | TEMP=74
 * |  15 | Post Humidity Data   |  ✓  |  ✓  |  ✓  | MAXH=40, MINH=35
 * |  16 | Get Humidity Data    |  ✓  |  ✓  |  ✓  | HUMIDITY=38
 * |  17 | Download Recipe File |  ✓  |  ✓  |  ✓  | RECIPE=FILENAME
 * |  18 | Start Recipe         |  ✓  |  ✓  |  ✓  | (NO COMMANDS) START IT
 * |  19 | Stop Recipe          |  ✓  |  ✓  |  ✓  | (NO COMMANDS) STOP IT
 */
class GVRequestHelper {
  constructor() {
    // Object of statuses and names
    const methods = [
      { status:  1, name: 'get_tree'             } ,
      { status:  2, name: 'set_system_time'      } ,
      { status:  3, name: 'post_system_time'     } ,
      { status:  4, name: 'get_system_time'      } ,
      { status:  5, name: 'post_wand_intensity'  } ,
      { status:  6, name: 'get_wand_intensity'   } ,
      { status:  7, name: 'post_pump_data'       } ,
      { status:  8, name: 'get_pump_data'        } ,
      { status:  9, name: 'post_valve_data'      } ,
      { status: 10, name: 'get_valve_data'       } ,
      { status: 11, name: 'post_dosing_data'     } ,
      { status: 12, name: 'get_dosing_data'      } ,
      { status: 13, name: 'post_hvac_data'       } ,
      { status: 14, name: 'get_hvac_data'        } ,
      { status: 15, name: 'post_humidity_data'   } ,
      { status: 16, name: 'get_humidity_data'    } ,
      { status: 17, name: 'download_recipe_file' } ,
      { status: 18, name: 'start_recipe'         } ,
      { status: 19, name: 'stop_recipe'          } ,
    ];

    // Dynamically create each method for helper object
    methods.forEach((method) => {
      this[method.name] = (options) => this.make_request(method.status, options);
    });
  }

  /*****************************************
   * Log request into gv_request table
   *****************************************/
  make_request(request_type, options) {
    const { silo, hub, device } = options;
    const request = options.request || null;

    // Crud validation
    return new Promise((resolve, reject) => {
      // Make sure required fields are specified else throw error
      // [ 'silo', 'hub', 'device' ].forEach((item) => {
      //   if(!options[item]) {
      //     throw Error(`${item} is a required parameter`);
      //   }
      // });
      // No error, proceed
      resolve();
    })
    // Successful validation
    .then(() => {
      // Create gv_request
      return GVRequests.create({
        silo,
        hub,
        device,
        request_type,
        request,
      });
    });
  }
}

export default new GVRequestHelper();
