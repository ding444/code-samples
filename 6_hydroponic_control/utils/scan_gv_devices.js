import {
  GVDevice,
  GVDeviceType,
  Silo,
  Hub,
  Device,
} from '../models';

// Get all devices and sort
GVDevice.findAll({
  include: GVDeviceType,
  order: [
    [ 'silo', 'ASC'  ],
    [ 'hub',  'ASC' ],
    [ 'type',  'ASC' ],
    [ 'device',  'ASC' ],
  ],
})

// Create silos in db
.then((devices) => {
  // Get silos from list of devices
  const silos = devices.filter((device) => device.type === 40);

  // Promise wrapper for all silo promises
  return new Promise((resolve, reject) => {

    // ForEach silo: create if doesn't exist
    Promise.all(silos.map((silo) =>
      Silo.findOrCreate({
        where: { MAC: silo.MAC },
        defaults: {
          order: silo.silo,
          status: silo.status,
          label: `Silo ${silo.silo--}`,
        }
      })
    ))

    // Array of silos
    .then((silos) => {
      // Get silo objects from array
      silos = silos.map((silo) => silo[0]);

      // Remove silos from devices
      const hubs_and_devices = devices.filter((device) => device.type !== 40);

      // Re-map silo_ids for devices
      const updated_devices = hubs_and_devices.map((device) => {
        // Get new silo_id
        const silo_id = silos.filter((silo) => silo.order == device.silo)[0].id;
        device.silo = silo_id; // Replace silo order with silo_id

        return device;
      });

      // Resolve with updated hubs and devices
      resolve(updated_devices);
    })

    .catch((error) => {
      console.error(error);
    })

  })
})

// Create hubs in db
.then((devices) => {
  // Get hubs from list of devices
  const hubs = devices.filter((device) => [41, 45, 46].includes(device.type));

  // Promise wrapper for all hub promises
  return new Promise((resolve, reject) => {

    // ForEach hub: create if doesn't exist
    Promise.all(hubs.map((hub) =>
      Hub.findOrCreate({
        where: { MAC: hub.MAC },
        defaults: {
          silo_id: hub.silo,
          order: hub.hub,
          status: hub.status,
          label: `Hub ${hub.hub--}`,
        },
      })
    ))

    // Continue with array of hubs
    .then((hubs) => {
      // Get hub objects from array
      hubs = hubs.map((hub) => hub[0]);

      // Remove hubs from devices
      const just_devices = devices.filter((device) => ![41, 45, 46].includes(device.type));

      // Update hub_id of remaining devices
      const new_devices = just_devices.map((device) => {
        // Get hub id
        const hub_id = hubs.filter((hub) =>
          // Get hub_id where device silo_id and hub.order match
          hub.order == device.hub && hub.silo_id == device.silo
        )[0].id;
        device.hub = hub_id; // Replace hub order with hub_id

        return device;
      });

      resolve(new_devices);
    })

    .catch((error) => {
      console.error(error);
    })

  })
})

// Create devices in db
.then((devices) => {
  // Promise wrapper for device promises
  return new Promise((resolve, reject) => {

    // Try creating devices
    Promise.all(devices.map((device) =>
      Device.findOrCreate({
        where: { MAC: device.MAC },
        defaults: {
          hub_id: device.hub,
          device_type_id: device.type,
          order: device.device,
          status: device.status,
          reading: device.reading,
        },
      })
    ))

    // Done -> continue
    .then((devices) => {
      resolve(devices);
    })

    .catch((error) => {
      console.error(error);
    })

  });
})

// Finished! exit
.then((devices) => {
  process.exit();
})

// Catch any errors and output to console
.catch((error) => {
  console.error(error);
})
