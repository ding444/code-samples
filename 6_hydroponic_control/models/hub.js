import { STRING, INTEGER } from 'sequelize';
import sequelize from '../utils/sequelize';
import Silo from './silo';
import Device from './device';

const Hub = sequelize.define('hub', {
  silo_id: {
    type: INTEGER,
    allowNull: false,
  },
  MAC: {
    type: STRING(8),
    allowNull: false,
  },
  order: INTEGER,
  status: INTEGER,
  label: STRING(128),
}, {
  createdAt: false,
  updatedAt: false,
  underscored: true,
  underscoredAll: true,
  freezeTableName: true,
});

Hub.belongsTo(Silo);
Hub.hasMany(Device);

export default Hub;
