import sequelize from '../utils/sequelize';

const CropHub = sequelize.define('crop_has_hub', {}, {
  timestamps: false,
  tableName: 'crop_has_hub',
});

export default CropHub;
