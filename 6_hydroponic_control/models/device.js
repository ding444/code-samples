import { STRING, INTEGER, DATE } from 'sequelize';
import sequelize from '../utils/sequelize';
import Notification from './notification';
import DeviceType from './device_type';

const Device = sequelize.define('device', {
  device_type_id: {
    type: INTEGER,
    allowNull: false,
  },
  MAC: STRING(12),
  order: INTEGER,
  status: INTEGER,
  reading: STRING(1024),
  ignored_until: DATE,
}, {
  createdAt: false,
  updatedAt: false,
  underscored: true,
  underscoredAll: true,
  freezeTableName: true,
});

Device.hasMany(Notification, {
  sourceKey: 'MAC',
  foreignKey: 'MAC',
});

Device.belongsTo(DeviceType);

export default Device;
