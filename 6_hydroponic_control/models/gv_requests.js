import { STRING, INTEGER, DATE } from 'sequelize';
import sequelize from '../utils/sequelize';
import GVResponses from './gv_responses';

const GVRequests = sequelize.define('gv_requests', {
  silo: INTEGER,
  hub: INTEGER,
  device: INTEGER,
  request_type: INTEGER,
  request: STRING(1024),
  read_at: DATE,
  processed_at: DATE,
  completed_at: DATE,
}, {
  updatedAt: false,
  underscored: true,
  underscoredAll: true,
  freezeTableName: true,
});

GVRequests.hasOne(GVResponses);

export default GVRequests;
