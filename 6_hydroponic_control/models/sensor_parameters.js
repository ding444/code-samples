import { STRING, INTEGER } from 'sequelize';
import sequelize from '../utils/sequelize';

const SensorParameters = sequelize.define('sensor_parameters', {
  sensor_type: STRING,
  sensor_max: INTEGER,
  sensor_min: INTEGER,
}, {
  createdAt: false,
  updatedAt: false,
  freezeTableName: true,
  underscored: true,
  underscoredAll: true,
});

export default SensorParameters;
