import { INTEGER } from 'sequelize';
import sequelize from '../utils/sequelize';
import HVACType from './hvac_type';

const SiloHVAC = sequelize.define('silo_hvac', {
  hvac_type_id: {
    type: INTEGER,
    allowNull: false,
  },
  min: INTEGER,
  max: INTEGER,
},
{
  createdAt: false,
  updatedAt: false,
  underscored: true,
  underscoredAll: true,
  freezeTableName: true,
});

SiloHVAC.belongsTo(HVACType);

export default SiloHVAC;
