import { STRING, INTEGER } from 'sequelize';
import sequelize from '../utils/sequelize';

const GVResponses = sequelize.define('gv_responses', {
  gv_request_id: {
    type: INTEGER,
    allowNull: false,
  },
  type: {
    type: INTEGER,
    allowNull: false,
  },
  request_status: {
    type: INTEGER,
    allowNull: false,
  },
  response: STRING,
}, {
  updatedAt: false,
  underscored: true,
  underscoredAll: true,
  freezeTableName: true,
});

export default GVResponses;
