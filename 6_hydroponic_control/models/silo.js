import { STRING, INTEGER } from 'sequelize';
import sequelize from '../utils/sequelize';
import SiloHVAC from './silo_hvac';

const Silo = sequelize.define('silo', {
  MAC: STRING(12),
  order: INTEGER,
  status: INTEGER,
  label: STRING,
}, {
  createdAt: false,
  updatedAt: false,
  underscored: true,
  underscoredAll: true,
  freezeTableName: true,
});

Silo.hasMany(SiloHVAC);

export default Silo;
