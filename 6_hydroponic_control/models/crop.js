import { STRING, INTEGER, DATE, NOW } from 'sequelize';
import sequelize from '../utils/sequelize';
import Recipe from './recipe';
import Stage from './stage';
import Hub from './hub';
import CropHub from './crop_hub';
// import Silo from './silo';

const Crop = sequelize.define('crop', {
  start_at: {
    type: DATE,
    defaultValue: NOW,
  },
  finish_at: {
    type: DATE,
    allowNull: false,
  },
  current_stage_id: {
    type: INTEGER,
    allowNull: false,
  },
  // recipe_plant_type: STRING,
  current_phase_id: INTEGER,
  next_stage_at: DATE,
  next_phase_at: DATE,
  yield: INTEGER,
  notes: STRING(1028),
  harvest_notes: STRING(1028),
  completed_at: DATE,
  harvested_at: DATE,
}, {
  updatedAt: false,
  underscored: true,
  underscoredAll: true,
  freezeTableName: true,
});


Crop.belongsTo(Recipe);

Crop.belongsTo(Stage, {
  as: 'current_stage',
  foreignKey: 'current_stage_id',
});

Crop.belongsToMany(Hub, { through: CropHub });

export default Crop;
