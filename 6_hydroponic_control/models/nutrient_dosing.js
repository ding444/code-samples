import { INTEGER } from 'sequelize';
import sequelize from '../utils/sequelize';

const NutrientDosing = sequelize.define('nutrient_dosing', {
  a: INTEGER,
  b: INTEGER,
  c: INTEGER,
  d: INTEGER,
}, {
  updatedAt: false,
  createdAt: false,
  underscored: true,
  underscoredAll: true,
  freezeTableName: true,
});

export default NutrientDosing;
