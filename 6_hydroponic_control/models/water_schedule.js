import { INTEGER } from 'sequelize';
import sequelize from '../utils/sequelize';

const WaterSchedule = sequelize.define('water_schedule', {
  duration: INTEGER,
  rest_period: INTEGER,
  nutrient_a: INTEGER,
  nutrient_b: INTEGER,
  nutrient_c: INTEGER,
  nutrient_d: INTEGER,
  ph_max: INTEGER,
  ph_min: INTEGER,
}, {
  updatedAt: false,
  createdAt: false,
  underscored: true,
  underscoredAll: true,
  freezeTableName: true,
});

export default WaterSchedule;
