import { STRING, INTEGER, ENUM } from 'sequelize';
import sequelize from '../utils/sequelize';
import Stage from './stage';
import SensorParameters from './sensor_parameters';
import RecipeHVAC from './recipe_hvac';

const Recipe = sequelize.define('recipe', {
  name: {
    type: STRING,
    allowNull: false,
  },
  plant_type: STRING,
  preset: INTEGER,
  status: {
    type: ENUM('active', 'archived'),
    defaultValue: 'active',
  },
  total_duration: INTEGER,
  derived_id: INTEGER,
}, {
  updatedAt: false,
  underscored: true,
  underscoredAll: true,
  freezeTableName: true,
});

Recipe.hasMany(Stage);
Recipe.hasMany(SensorParameters);
Recipe.hasMany(RecipeHVAC);

export default Recipe;
