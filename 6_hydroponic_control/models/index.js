// Agregate models together
import Recipe from './recipe';
import Stage from './stage';
import LightPhase from './light_phase';
import WaterSchedule from './water_schedule';
import NutrientDosing from './nutrient_dosing';
import Crop from './crop';
import SensorParameters from './sensor_parameters';
import Silo from './silo';
import Hub from './hub';
import CropHub from './crop_hub';
import Device from './device';
import GVRequests from './gv_requests';
import GVResponses from './gv_responses';
import Notification from './notification';
import DeviceType from './device_type';
import HVACType from './hvac_type';
import RecipeHVAC from './recipe_hvac';
import SiloHVAC from './silo_hvac';
import GVDevice from './gv_device';
import GVDeviceType from './gv_device_type';

// Some associations
Silo.hasMany(Hub);
Hub.belongsToMany(Crop, { through: CropHub });
Notification.belongsTo(Device, {
  targetKey: 'MAC',
  foreignKey: 'MAC',
});
Device.belongsTo(Hub);

export {
  Recipe,
  Stage,
  LightPhase,
  WaterSchedule,
  NutrientDosing,
  Crop,
  SensorParameters,
  Silo,
  Hub,
  CropHub,
  Device,
  GVRequests,
  GVResponses,
  Notification,
  DeviceType,
  RecipeHVAC,
  SiloHVAC,
  HVACType,
  GVDevice,
  GVDeviceType,
}
