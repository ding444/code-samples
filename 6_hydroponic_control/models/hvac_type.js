import { STRING } from 'sequelize';
import sequelize from '../utils/sequelize';

const HVACType = sequelize.define('hvac_type', {
  type: {
    type: STRING,
    allowNull: false,
  },
  label: STRING,
},
{
  createdAt: false,
  updatedAt: false,
  underscored: true,
  underscoredAll: true,
  freezeTableName: true,
});

export default HVACType;
