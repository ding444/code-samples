import { STRING } from 'sequelize';
import sequelize from '../utils/sequelize';

const GVDeviceType = sequelize.define('gv_device_type', {
  type: STRING,
},
{
  createdAt: false,
  updatedAt: false,
  underscored: true,
  underscoredAll: true,
  freezeTableName: true,
});

export default GVDeviceType;
