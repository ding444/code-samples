import { STRING } from 'sequelize';
import sequelize from '../utils/sequelize';

const DeviceType = sequelize.define('device_type', {
  type: STRING,
}, {
  createdAt: false,
  updatedAt: false,
  underscored: true,
  underscoredAll: true,
  freezeTableName: true,
});

export default DeviceType;
