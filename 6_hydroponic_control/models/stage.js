import { STRING, INTEGER } from 'sequelize';
import sequelize from '../utils/sequelize';
import LightPhase from './light_phase';
import WaterSchedule from './water_schedule';
import SensorParameters from './sensor_parameters';

const Stage = sequelize.define('stage', {
  name: {
    type: STRING,
    allowNull: false,
  },
  order: INTEGER,
  cycles: INTEGER,
  duration: INTEGER,
}, {
  updatedAt: false,
  createdAt: false,
  underscored: true,
  underscoredAll: true,
  freezeTableName: true,
});

// I can't figure out why this fails yet but
// importing the Recipe model yields null.
// There is something weird going on - Dean
// Stage.belongsTo(Recipe);

Stage.hasMany(LightPhase);
Stage.hasOne(WaterSchedule);
Stage.hasMany(SensorParameters);

export default Stage;
