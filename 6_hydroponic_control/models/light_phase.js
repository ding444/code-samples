import { INTEGER } from 'sequelize';
import sequelize from '../utils/sequelize';

const LightPhase = sequelize.define('light_phase', {
  order: INTEGER,
  duration: INTEGER,
  warm: INTEGER,
  cool: INTEGER,
  red: INTEGER,
  blue: INTEGER,
}, {
  updatedAt: false,
  createdAt: false,
  underscored: true,
  underscoredAll: true,
  freezeTableName: true,
});

// LightPhase.belongsTo(Stage);

export default LightPhase;
