import { STRING, INTEGER } from 'sequelize';
import sequelize from '../utils/sequelize';
import GVDeviceType from './gv_device_type';

const GVDevice = sequelize.define('gv_devices', {
  type: INTEGER,
  MAC: STRING,
  silo: INTEGER,
  hub: INTEGER,
  device: INTEGER,
  // parent_mac: STRING,
  parent_id: STRING,
  status: INTEGER,
  reading: STRING,
},
{
  createdAt: false,
  updatedAt: false,
  underscored: true,
  underscoredAll: true,
  freezeTableName: true,
});

GVDevice.belongsTo(GVDeviceType, {
  foreignKey: 'type',
});

export default GVDevice;
