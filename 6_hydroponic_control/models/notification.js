import { STRING, DATE } from 'sequelize';
import Sequelize from '../utils/sequelize';

const Notification = Sequelize.define('notifications', {
  type:     STRING(64),
  status:   STRING(32),
  message:  STRING(512),
  MAC:      STRING(12),
  read_at:  DATE,
},
{
  createdAt: false,
  updatedAt: false,
  underscored: true,
  underscoredAll: true,
  freezeTableName: true,
});

export default Notification;
