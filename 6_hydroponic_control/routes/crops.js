import { Router } from 'express';
import { CropController } from '../controllers';

const crops = Router();

/*****************************************
 * Crop API Routes
 * Base: /api/crops
 *****************************************/
crops.get('/pods', CropController.get_available_pods); // Download recipe
crops.post('/', CropController.start);                 // Start/create crop
crops.post('/harvest', CropController.harvest);        // Record harvest
crops.get('/:id/download', CropController.download);   // Download recipe
crops.get('/:qualifier*?', CropController.get);        // Get all, history, active, or singular crops

export default crops;
