import { Router } from 'express';
import { GVLinkController } from '../controllers';

const gvlink = Router();

/*****************************************
 * Recipe API Routes
 * Base: /api/recipe
 *****************************************/
gvlink.get('/build_tree', GVLinkController.build_tree); // List all silos
gvlink.get('/get_tree', GVLinkController.get_tree);     // List all silos

// Catchall matches /get_valve_data/silo/4/hub/3/device1
const catchall_regex = /^\/\w+_\w+\/silo\/([^\\\/]+?)\/hub\/([^\\\/]+?)\/device\/([^\\\/]+?)(?:\/(?=$))?$/i;
gvlink.all(catchall_regex, GVLinkController.make_generic_request);

export default gvlink;
