import { Router } from 'express';
import { SiloController } from '../controllers';

const silos = Router();

/*****************************************
 * Recipe API Routes
 * Base: /api/recipe
 *****************************************/
silos.get('/', SiloController.get);                                  // List all silos
silos.get('/:id', SiloController.get);                               // Get single silo
silos.get('/:id/pods?', SiloController.getPods);                     // Get silo's pods
silos.get('/:id/Notifications?', SiloController.getNotifications);   // Get silo's notifications
silos.get('/:id/devices?', SiloController.getDevices);               // Get silo's devices
silos.get('/:id/devices?/hub/:hub_id', SiloController.getDevices);   // Get silos's devices by hub
silos.get('/:id/devices?/crop/:crop_id', SiloController.getDevices); // get silo's devices by crop
silos.get('/:id/devices_by_type', SiloController.getDevicesByType);  // get devices by type

export default silos;
