import { Router } from 'express';
import { RecipeController } from '../controllers';

const recipes = Router();

/*****************************************
 * Recipe API Routes
 * Base: /api/recipe
 *****************************************/
recipes.post('/', RecipeController.create);         // Create recipe
recipes.put('/:id',RecipeController.create);        // Update recipe
recipes.delete('/:id',RecipeController.delete);     // Delete recipe
recipes.get('/:qualifier*?', RecipeController.get); // Get all, active, archived, or singular recipes

export default recipes;
