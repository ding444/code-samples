import { Router } from 'express';
import { NotificationsController } from '../controllers';

const notifications = Router();

/*****************************************
 * Notifications API Routes
 * Base: /api/notifications
 *****************************************/
notifications.get('/', NotificationsController.get);         // List all notifications
notifications.get('/:id', NotificationsController.get);      // Get single notification

export default notifications;
