import { Router } from 'express';
import bodyParser from 'body-parser';
import recipes from './recipes';
import crops from './crops';
import silos from './silos';
import notifications from './notifications';
import gvlink from './gvlink';
// import cultivars from './cultivars';

const api = Router();

// json parser for /api route
api.use(bodyParser.json());
api.use(bodyParser.urlencoded({ extended: true }));

// Load external routes.  Regex for plural or singular endpoint
api.use('/recipes?', recipes);
api.use('/crops?', crops);
api.use('/silos?', silos);
api.use('/notifications?', notifications);
api.use('/gvlinks?', gvlink);
// app.use('/cultivars?', cultivars);

api.get('/', function (req, res){
  res.json({
    status:200,
    message: 'API is running'
  });
});

export default api;
