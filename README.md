# Sample Code Repo

## Introduction

These are few of my _semi_-recent projects.  I am unable to include some of my newest ones which show more of my current coding style ([Airbnb Javascript Style Guide](https://github.com/airbnb/javascript), ES6 Syntax, Webpack, etc) because I don't have permission to share them.  I have included parts of the projects which I am permitted to share in the [Miscellaneous](5_misc_files) folder.

Most of them are JavaScript projects because I think they are most relevant for this position, but I can provide projects that utilize other languages or tech upon [request](mailto:dean@four4four.com?subject=Please%20provide%20more%20examples%20).

## Projects
1. [**Audit Fiber Script**](1_audit_fiber_script) - I write a lot of scripts to solve problems and this is one that bridges a gap between one of our wholesaler's system and our billing system.
2. [**Yakima Protocol App**](2_yakima_ems_protocol_app) - This is a mobile app that uses the Ionic Framework (AngularJS essentially)
3. [**Moisture Monitoring**](3_moisture_monitoring) - An agricultural project that facilitates smarter crop irrigation through a variety of metrics.
4. [**SMW Phone**](4_smw_phone/README.md) - A business IVR phone system built on Twilio that handles call queueing, SIP integration, call metrics, and more.
5. [**Miscellaneous files**](5_misc_files) - I included small parts of more current projects that I've been working on
6. [**Hydroponic Control**](6_hydroponic_control) - Part of a large-scale hydroponic farm control system

