// REQUIRES
var express		= require('express'),
	bodyParser	= require("body-parser");
	session		= require('express-session')
	RedisStore	= require('connect-redis')(session)
	mysql		= require('mysql')
	cors		= require('cors')
	compression	= require('compression')
	router		= express.Router()
	async		= require('async')
	moment		= require('moment')
	winston		= require('winston')
	settings	= require('./settings.js')
	spawn		= require('child_process').spawn
	schedule	= require('node-schedule');





// WINSTON LOGGING SETTINGS

var logger = new (winston.Logger)({
	transports: [
		new (winston.transports.Console)({
			handleExceptions: true
		}),
		new (winston.transports.File)({
			filename: 'logs/log.log',
			 handleExceptions: true
		})
	],
	// exitOnError: false
});



// MYSQL CONNECTION PARAMETERS
var connection = mysql.createConnection({
	// debug: true,
	host		: settings.mysql.host || 'localhost',
	user		: settings.mysql.user || 'root',
	password	: settings.mysql.password || '',
	database	: settings.mysql.database
});


//EXPRESS SETTINGS
var app = express();

app.use(compression());
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json());
app.use(cors());
app.use(express.static('static'));
app.use(session({
	store: new RedisStore(),
	secret: 'supersecretsecretforsecrets',
	resave: false,
	saveUninitialized: false,
	ttl: 604800
}));

var auth = function(req, res, next){
	if(req.session.auth)
		next();
	else
	{
		// res.status(401).send('You must be logged in');
		// logger.info('Failed request %s by %s', req.url, req.ip);
		var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
		logger.info('Failed request %s by %s', req.url, ip);
		res.status(401).redirect('/');
	}
}


// EXPRESS ROUTES

app.get('/', function(req, res, next){
	 res.sendFile(__dirname + '/static/login.html');
});


app.all('/login', function(req, res, next){
	var user = req.body.user || false;
	var pass = req.body.pass || false;

	if(user == false || pass == false)
	{
		var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
		logger.info('Failed login attempt for user %s by %s', user, ip);
		res.redirect('/');
		return
	}

	connection.query('SELECT * FROM users WHERE username = ? LIMIT 1', [user], function(error, result){
		if(result.length != 1)
		{
			var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
			logger.warn('Failed login attempt for user %s by %s', user, ip);
			res.redirect('/');
			return;
		}

		result = result[0];

		if(result.password == pass)
		{
			var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
			logger.info('Successfull login by %s from %s', user, ip);

			req.session.auth = {
				id: result.id,
				user: result.username,
				name: result.name,
				level: result.level
			};
			res.redirect('/main');
		}
		else
		{
			var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
			logger.warn('Failed login attempt for user %s by %s', user, ip);
			req.session.destroy();
			res.redirect('/');
		}
	})

});

app.get('/logout', function(req, res, next){
	req.session.destroy();
	res.redirect('/');
});

app.get('/settings', auth, function(req, res, next){
	connection.query('SELECT a.*, b.type FROM units AS a LEFT JOIN crops AS b on a.crop = b.id', function(err, rows) {
		if(err)
		{
			logger.error('Error executing myql query for %s from %s by %s', req.url, req.ip, req.session.auth.user, err);
		}
		res.send({
			user: req.session.auth,
			units: rows,
			order: {
				'27': 2,
				'28': 1,
				'29': 3
			}
		});
	});
});

app.get('/main', auth, function(req, res, next){
	 res.sendFile(__dirname + '/static/main.html');
});

app.get('/get_units', auth, function(req, res, next) {
	connection.query('SELECT a.*, b.type FROM units AS a LEFT JOIN crops AS b on a.crop = b.id', function(err, rows) {
		if(err)
		{
			logger.error('Error executing myql query for %s from %s by %s', req.url, req.ip, req.session.auth.user, err);
		}
		res.json(rows);
	});
});

app.get('/bar', auth, function(req, res, next) {
	req.session.test = req.session.test || 0;
	req.session.test++;
	res.send('incremented session');
});

// app.get('/foo', auth, function (req, res, next) {
// 	res.send('test '+ req.session.test);
// });

app.route('/hand')
	.get(auth, function(req, res, next){
		var date = moment().format('YYYY-MM-DD');
		res.redirect('/hand/'+ date);
	})
	.put(auth, function (req, res, next) {
		var user_id = req.session.auth.id || false;
		var date = req.body.date || false;
		var units = req.body.units || false;

		if(user_id == false)
		{
			res.status(401).send({msg: 'Must be logged in'});
			return;
		}
		if(date == false)
		{
			res.status(400).send({msg: 'Invalid date'});
			return;
		}
		if(units == false)
		{
			res.status(400).send({msg: 'Invalid units'});
			return;
		}

		date = moment(date).format('YYYY-MM-DD');

		var data = [];
		for(var key in units){
			data.push([date, user_id, key, units[key]]);
		}

		var sql = 'INSERT INTO hand_readings (reading_date, user, unit, value) VALUES ? ON DUPLICATE KEY UPDATE value=VALUES(value)';

		connection.query(sql, [data], function(err, results) {
			if(err) {
				logger.error('Error executing myql query for %s from %s by %s', req.url, req.ip, req.session.auth.user, err);
				res.status(500).send('There was an error updating the database');
			}
			res.send({msg:'Hand data successfully saved'});
		});
	});

app.route('/hand/:date')
	.get(auth, function(req, res, next) {
		var user_id = req.session.auth.id || false;
		var date = req.params.date || false;

		if(user_id == false)
		{
			res.status(401).send({msg: 'Must be logged in'});
			return;
		}
		if(date == false)
		{
			res.status(400).send({msg: 'Invalid date'});
			return;
		}

		date = moment(date).format('YYYY-MM-DD');
		
		connection.query('SELECT * FROM hand_readings WHERE reading_date = ? AND user = ?', [date, user_id], function(err, results) {
			if(err)
			{
				logger.error('Error executing myql query for %s from %s by %s', req.url, req.ip, req.session.auth.user, err);
			}
			var units = {};
			for(var i = 0, l = results.length; i < l; i++){
				units[results[i].unit] = results[i].value;
			};

			res.send({
				units: units,
				msg:'ss'
			});
		});

	});

app.route('/irrigation')
	.get(auth, function(req, res, next){
		var date = moment().format('YYYY-MM-DD');
		res.redirect('/irrigation/'+ date);
	})
	.put(auth, function (req, res, next) {
		var user_id = req.session.auth.id || false;
		var date = req.body.date || false;
		var units = req.body.units || false;

		if(user_id == false)
		{
			res.status(401).send({msg: 'Must be logged in'});
			return;
		}
		if(date == false)
		{
			res.status(400).send({msg: 'Invalid date'});
			return;
		}
		if(units == false)
		{
			res.status(400).send({msg: 'Invalid units'});
			return;
		}

		date = moment(date).format('YYYY-MM-DD');

		var data = [];
		for(var key in units){
			data.push([date, key, units[key]]);
		}

		var sql = 'INSERT INTO irrigation_schedule (irrigation_date, unit, on_off) VALUES ? ON DUPLICATE KEY UPDATE on_off=VALUES(on_off)';

		connection.query(sql, [data], function(err, results) {
			if(err)
			{
				logger.error('Error executing myql query for %s from %s by %s', req.url, req.ip, req.session.auth.user, err);
				res.status(500).send('There was an error updating the database');
			}
			res.send({msg:'Irrigation data successfully saved'});
		});
	});

app.route('/irrigation/:date')
	.get(auth, function(req, res, next) {
		var user_id = req.session.auth.id || false;
		var date = req.params.date || false;

		if(user_id == false)
		{
			res.status(401).send({msg: 'Must be logged in'});
			return;
		}
		if(date == false)
		{
			res.status(400).send({msg: 'Invalid date'});
			return;
		}

		date = moment(date).format('YYYY-MM-DD');
		
		connection.query('SELECT * FROM irrigation_schedule WHERE irrigation_date = ?', [date], function(err, results) {
			if(err)
			{
				logger.error('Error executing myql query for %s from %s by %s', req.url, req.ip, req.session.auth.user, err);
			}
			var units = {};
			for(var i = 0, l = results.length; i < l; i++){
				units[results[i].unit] = results[i].on_off;
			};

			res.send({
				units: units,
				msg:'ss'
			});
		});

	});



app.route('/schedule/:unit')
	.put(auth, function (req, res, next) {
		var user_id = req.session.auth.id || false;
		var user_level = req.session.auth.level || false;
		var date = req.body.date || false;
		var unit = req.params.unit || false;
		var on_off = req.body.on_off;

		if(user_level < 1)
		{
			res.status(401).send({msg: 'Invalid permissions'});
			return;
		}
		if(date == false)
		{
			res.status(400).send({msg: 'Invalid date'});
			return;
		}
		if(unit == false)
		{
			res.status(400).send({msg: 'Invalid data'});
			return;
		}

		date = moment(date).format('YYYY-MM-DD');

		var data = [date, unit, on_off];

		var sql = 'INSERT INTO irrigation_future (date, unit, on_off) VALUES (?) ON DUPLICATE KEY UPDATE on_off=VALUES(on_off)';

		connection.query(sql, [data], function(err, results) {
			if(err) {
				logger.error('Error executing myql query for %s from %s by %s', req.url, req.ip, req.session.auth.user, err);
				res.status(500).send('There was an error updating the database');
			}
			res.send({msg:'Irrigation data successfully saved'});
		});
	})
	.get(auth, function(req, res, next) {
		var user_id = req.session.auth.id || false;
		var unit = req.params.unit || false;

		date = moment().format('YYYY-MM-DD');
		
		connection.query('SELECT * FROM irrigation_future WHERE unit = ? && date >= ?', [unit, date], function(err, results) {
			if(err) {
				res.status(500).send('There was an error updating the database');
				logger.error('Error executing myql query for %s from %s by %s', req.url, req.ip, req.session.auth.user, err);
			}
			var units = {};
			for(var i = 0, l = results.length; i < l; i++){
				var result_date = moment(results[i].date).format('YYYY-MM-DD');
				units[result_date] = {
					date: result_date,
					value: results[i].on_off
				};
			};

			res.send({
				dates: units,
				msg:'ss'
			});
		});
	});

app.get('/schedule', auth, function(req, res, next){
	date = moment().format('YYYY-MM-DD');
	
	connection.query('SELECT * FROM irrigation_future WHERE date >= ?', [date], function(err, results) {
		if(err) {
			res.status(500).send('There was an error updating the database');
			logger.error('Error executing myql query for %s from %s by %s', req.url, req.ip, req.session.auth.user, err);
		}
		connection.query('SELECT a.*, b.name FROM irrigation_approval AS a LEFT JOIN users AS b on a.user = b.id WHERE timestamp > ?', [date], function(err, approval_results){
			if(err) {
				res.status(500).send('There was an error updating the database');
				logger.error('Error executing myql query for %s from %s by %s', req.url, req.ip, req.session.auth.user, err);
			}

			var units = {};
			for(var i = 0, l = results.length; i < l; i++){
				var result_date = moment(results[i].date).format('YYYY-MM-DD');
				if(typeof units[results[i].unit] == "undefined")
					units[results[i].unit] = {};
				units[results[i].unit][result_date] = results[i].on_off;
			};

			res.send({
				units: units,
				msg:'ss',
				approval: approval_results[0]
			});
		});
	});
});


app.get('/graphs/hand/:unit', auth, function(req, res){
	var data = {
		users: {}
	};

	connection.query('SELECT a.*, b.name, b.id AS user_id FROM hand_readings AS a LEFT JOIN users AS b ON a.user = b.id WHERE reading_date > ? AND unit = ? ORDER BY reading_date ASC', [moment().subtract(31, 'days').format('YYYY-MM-DD'), req.params.unit], function(err, results){
		if(err)
			logger.error('Error executing myql query for %s from %s by %s', req.url, req.ip, req.session.auth.user, err);

		for(var i=0, l = results.length; i< l; i++)
		{
			if(typeof data.users[results[i].name] == 'undefined')
			{
				data.users[results[i].name] = {
					data: [],
					color: results[i].user_id
				};
			}

			data.users[results[i].name].data.push([
				moment(results[i].reading_date).startOf('day').format('x'),
				results[i].value
			]);
		}
		res.send(data);
	});
});

app.get('/graphs/irrigation/:unit', auth, function(req, res){
	var data = {
		data: []	
	};
	connection.query('SELECT * FROM irrigation_schedule WHERE irrigation_date > ? AND unit = ? ORDER BY irrigation_date ASC', [moment().subtract(23, 'days').format('YYYY-MM-DD'), req.params.unit], function(err, results){
		if(err) {
			logger.error('Error executing myql query for %s from %s by %s', req.url, req.ip, req.session.auth.user, err);
		}

		for(var i=0, l = results.length; i< l; i++)
		{
			data.data.push([
				moment(results[i].irrigation_date).startOf('day').format('x'),
				results[i].on_off
			]);
		}

		res.send(data);
	});
	
});

app.get('/graphs/weather/:unit', auth, function(req, res){
	var unit = req.params.unit;
	connection.query('SELECT * FROM units AS a LEFT JOIN locations AS b ON a.weather_location = b.id WHERE a.id = ?', [unit], function(err, results){
		var weather_table = results[0].table;
		connection.query('SELECT * FROM ?? WHERE date > DATE_SUB(CURRENT_TIMESTAMP, INTERVAL 22 day)', [weather_table], function(err, results){
			var data = {};
			data.temp = [];
			data.extremes = [];
			data.evapo = [];
			data.future_evapo = [];
			for(var i=0, l=results.length; i < l; i++)
			{
				var timestamp = moment(results[i].date).startOf('day').format('x');

				data.temp.push([
					timestamp,
					results[i].air_temp
				]);
				data.extremes.push([
					timestamp,
					results[i].min_temp,
					results[i].max_temp
				]);
				data.evapo.push([
					timestamp,
					results[i].evapotranspiration_grass
				]);
			}
			connection.query('SELECT * FROM forecast ORDER BY date DESC LIMIT 1', function(err, result){
				if(err) console.log(err);
				var today = moment().startOf('day');
				var reading_date = moment(result[0].date).startOf('day');
				var diff = today.diff(reading_date, 'days');
				for(i=diff+1; i < 6; i++)
				{
					var new_timestamp = moment(today).add(i-diff-1, 'days').format('x');

					data.future_evapo.push([
						new_timestamp,
						result[0]['evap_low_'+i],
						result[0]['evap_high_'+i]
					]);
					data.extremes.push([
						new_timestamp,
						result[0]['low'+i] != 'null' ? result[0]['low'+i] : data.extremes[data.extremes.length-1][1],
						result[0]['high'+i]
					]);
				}
				data.future_evapo.unshift([
					data.evapo[data.evapo.length-1][0],
					data.evapo[data.evapo.length-1][1],
					data.evapo[data.evapo.length-1][1]
				]);
				res.send(data);
			});
		});
	});
});

app.put('/new_user', auth, function(req, res){
	var user_level = req.session.auth.level;
	var username = req.body.user || false;
	var password = req.body.password || false;
	var name = req.body.name || false;


	if(user_level != 9)
	{
		res.status(401).send({msg: 'Invalid permissions'});
		return;
	}

	if(username == false || password == false || name == false)
	{
		res.status(400).send({msg: 'Invalid parameters'});
		return;
	}

	connection.query('INSERT INTO users (username, password, name, level) VALUES (?) ON DUPLICATE KEY UPDATE password=VALUES(password), name=VALUES(name)', [[username, password, name, 1]], function(err, result){
		if(err) {
			logger.error('Error executing myql query for %s from %s by %s', req.url, req.ip, req.session.auth.user);
			res.send({msg: 'Error: '+ err.code});
		}
		else{
			logger.info('User %s successfully added or modified by %s', username, req.session.auth.user, req.body)
			res.send({msg: 'User successfully added'});
		}
	});
});


app.put('/approve_irrigation', auth, function(req, res){
	var user_level = req.session.auth.level;
	var user_id = req.session.auth.id;

	if(user_level < 3)
	{
		res.status(401).send({msg: 'Invalid permissions'});
		return;
	}

	connection.query('INSERT INTO irrigation_approval (user) VALUES (?)', [user_id], function(err, results){
		if(err) {
			logger.error('Error executing myql query for %s from %s by %s', req.url, req.ip, req.session.auth.user);
			res.send({msg: 'Error: '+ err.code});
		}
		connection.query('SELECT a.*, b.name FROM irrigation_approval AS a LEFT JOIN users AS b on a.user = b.id WHERE a.id = ?', [results.insertId], function(err, result){
			if(err) {
				logger.error('Error executing myql query for %s from %s by %s', req.url, req.ip, req.session.auth.user);
				res.send({msg: 'Error: '+ err.code});
			}

			res.send({
				msg: 'Irrigation successfully approved',
				approval: result[0]
			});
		});

	});

});



app.get('/logs/:unit', auth, function(req, res){
	var unit = req.params.unit;
	connection.query('SELECT users.name, logs.* FROM logs LEFT JOIN users ON logs.user = users.id WHERE unit = ? ORDER BY datetime DESC', [unit], function(err, result){
		if(err) {
			logger.error('Error executing myql query for %s from %s by %s', req.url, req.ip, req.session.auth.user);
			res.send({msg: 'Error: '+ err.code});
		}
		res.send({msg: 'Success', logs: result});
	});
});



// var daily_weather = schedule.scheduleJob({hour: 3, minute: 0}, function(){
// 	var sh = spawn('node', ['./get_daily_weather.js']);
// 	console.log('Getting daily weather');
// });



// START SERVER
var server = app.listen(settings.web.port || 3000, function () {
  var host = server.address().address;
  var port = server.address().port;

  console.log('Server started at http://%s:%s', host, port);
  logger.info('Server listning at %s:%s', host, port);
});
