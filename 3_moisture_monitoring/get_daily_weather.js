var request = require('request');
var moment = require('moment');
var mysql = require('mysql');
var async = require('async');
var settings = require('./settings.js');

var connection = mysql.createConnection({
	// debug: true,
	host		: settings.mysql.host || 'localhost',
	user		: settings.mysql.user || 'root',
	password	: settings.mysql.password || '',
	database	: settings.mysql.database
});

connection.connect();

request({
	method: 'GET',
	url: 'http://weather.prosser.wsu.edu/webservice/stationdata',
	qs: {
		START: moment().startOf('day').subtract(7, 'days').format('YYYY-MM-DD HH:mm:ss'),
		BASIS: 'DAILY'
	}
}, (err, response, body) => {
	var data = JSON.parse(body);
	if(data.status !== 1)
		console.log('Invalid request');
		process.exit();

	async.each(data.message, (item, callback) => {

		switch(item.STATION_NAME) {
			case 'Royal City East':
				var table = 'royal_daily_weather';
				break;
			case 'Mattawa':
				var table = 'mattawa_daily_weather';
				break;
		}
		
		let insert_data = [];

		item.DATA.reverse();
		item.DATA.forEach((line) => {
			var {JULDATE_PST: date, MIN_AT_F: min_temp, AVG_AT_F: avg_temp, MAX_AT_F: max_temp, ETO: et_grass, ETR: et_alfalfa} = line;

			var line_obj = {
				date: date,
				min_temp: min_temp,
				air_temp: avg_temp,
				max_temp: max_temp,
				evapotranspiration_grass: et_grass,
				evapotranspiration_alfalfa: et_alfalfa
			}

			insert_data.push([date, min_temp, avg_temp, max_temp, et_grass, et_alfalfa]);
		});

		var query = connection.query('INSERT IGNORE INTO '+ table +' (date, min_temp, air_temp, max_temp, evapotranspiration_grass, evapotranspiration_alfalfa) VALUES ?', [insert_data], function(err, results){
			if(err) console.log(err);
			// console.log(results);
			// console.log(query.sql);
			callback();
		});
	},
	(err) => {
		connection.end();
	});
});
