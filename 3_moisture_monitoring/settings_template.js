var config = {}

config.mysql = {};
config.mysql.user = 'root';
config.mysql.password = '';
config.mysql.database = 'moisture_monitoring';


config.web = {};
config.web.port = 3000;

module.exports = config;
