# Moisture Monitoring

## Introduction

This is an older version (that I have permission to share) of our Moisture Monitoring project.  It uses a combination of weather, soil moisture, and human metrics to enable the user to make smarter irrigation decisions on their crops.  Some of it is a little old and the newer version has newer functionality, refactored improvements, more ES6 syntax, and better commenting.

### Tech used

* Node.js + Express backend
* Bower
* MySQL
* Angular + Bootstrap responsive front-end
* Weather scraping script + weather API script 
	- They only recently added API access so previously I had to be creative with scraping the weather data

# Why I think it's cool

This project has always been fun and we've been modifying the system for the last 8 years.  We have gone through numerous technologies and settled on this for now.  I have always had the opportunity to be very hands on with this project with both the hardware and sofware.

We have probes that we deploy in our crop fields that measure precipitation and soil moisture.  We put together our own hardware and software for this project.
