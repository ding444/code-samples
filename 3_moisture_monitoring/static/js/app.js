var moistureApp = angular.module('moistureApp', ['moistureControllers', 'ui.router','ui.bootstrap', 'ngAnimate', 'angularMoment', 'highcharts-ng', 'ui.sortable']);

moistureApp.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', function($stateProvider, $urlRouterProvider, $locationProvider){

	moistureApp.constant('angularMomentConfig', {
	  preprocess: 'utc',
	  timezone: 'America/Los_Angeles'
	});

	$stateProvider
		.state('dashboard', {
			url: '/dashboard',
			templateUrl: '/templates/dashboard.html'
		})
		.state('hand_readings', {
			url: '/hand_readings',
			controller: 'handReadings',
			templateUrl: '/templates/hand_readings.html'
		})
		.state('irrigation', {
			url: '/irrigation',
			controller: 'irrigationController',
			templateUrl: '/templates/irrigation.html'
		})
		.state('unit', {
			url: '/unit/:unit',
			controller: 'unitController',
			templateUrl: '/templates/unit.html'
		})
		.state('review', {
			url: '/review',
			controller: 'reviewController',
			templateUrl: '/templates/review.html'
		})
		.state('add_user', {
			url: '/add_user',
			controller: 'addUserController',
			templateUrl: '/templates/add_user.html'
		})
		.state('unit_order', {
			url: '/unit_order',
			controller: 'unitOrderController',
			templateUrl: '/templates/unit_order.html'
		});

		$urlRouterProvider.otherwise("/dashboard");
		// $locationProvider.html5Mode(true);
}]);

moistureApp.run(['$rootScope', 'user', '$timeout', function($rootScope, user, $timeout){
	$('#myNavmenu').offcanvas({toggle: false, canvas: 'body'});
	$('#bs-example-navbar-collapse-1').collapse({toggle: false});

	$rootScope.user_name = user.name;
	$rootScope.user_level = user.level;

	$rootScope.$on('$stateChangeSuccess', function (event, next, current) {
		$('#myNavmenu').offcanvas('hide');
		$('#bs-example-navbar-collapse-1').collapse('hide');
		$("html, body").animate({ scrollTop: 0 }, 200);
	});

	$rootScope.alerts = [];
	$rootScope.showAlert = function(msg, settings)
	{

		var settings = settings || {};
		var type = settings.type || 'warning';
		var autoclose = settings.autoclose || true;
		var time = settings.time || 3000;
		var title = settings.title || false;

		$rootScope.alerts.push({type:type, msg:msg, title:title});
		if(autoclose)
			$timeout(function(){
				$rootScope.closeAlert(0,1);
			}, time)
	}

	$rootScope.closeAlert = function(index) {
		$rootScope.alerts.splice(index, 1);
	};
}]);

moistureApp.filter('capitalize', function(){
	return function(input, scope) {
		if (input!=null)
			input = input.toLowerCase();
		return input.substring(0,1).toUpperCase()+input.substring(1);
	}
});


function fetchData() {
	var initInjector = angular.injector(["ng"]);
	var $http = initInjector.get("$http");

	return $http.get('/settings').then(function(response) {
		order = response.data.order;
		console.log(order);

		units = response.data.units.sort(function(a,b){
			var id1 = order[a.id] || a.id;
			var id2 = order[b.id] || b.id;
			// return a.id - b.id;
			return id1 - id2;
		});
		moistureApp.constant('units', units);
		moistureApp.constant('user', response.data.user);

	}, function(error) {
		console.log('error');
		console.log(error);
		window.location.replace('/');
		// Handle error case
	});
}

function bootstrapApplication() {
	angular.element(document).ready(function() {
		angular.bootstrap(document, ['moistureApp']);
	});
}

fetchData().then(bootstrapApplication);
