var moistureControllers = angular.module('moistureControllers', []);

moistureControllers.controller('handReadings', ['$scope', '$http', 'units', '$rootScope', '$filter', function($scope, $http, units, $rootScope, $filter){
	$scope.units = units;
	$scope.dt = new Date();
	$scope.max_date = new Date();
	$scope.originalValues;

	$scope.submit = function(){
		// if(Object.keys($scope.radioModel).length < 1)
		if(angular.equals($scope.radioModel, $scope.originalValues))
			$rootScope.showAlert('Values are not different', {type: 'warning', title: 'Error'});
		else
		{
			$http.put('/hand', {
				units: $scope.radioModel,
				date: $filter('date')($scope.dt, 'yyyy-MM-dd')
			})
				.success(function(data){
					$rootScope.showAlert(data.msg, {type: 'success', title: 'Sucess'});
					$scope.originalValues = angular.copy($scope.radioModel);
				})
				.error(function(error){
					console.log(error);
					$rootScope.showAlert(data.msg, {type: 'danger', title: 'Error'});
				});
		}
	}

	$scope.updateDate = function(){
		var filtered_date = $filter('date')($scope.dt, 'yyyy-MM-dd');
		$http.get('/hand/'+ filtered_date, {date: 'today'})
			.success(function(data){
				$scope.originalValues = angular.copy(data.units);
				$scope.radioModel = data.units;
			})
			.error(function(error){
				$rootScope.showAlert(data.msg);
			});
	}

	$scope.$watch('dt', function(newDate){
		$scope.updateDate();
	});
}])

.controller('unitController', ['$scope', '$stateParams', 'units', '$http', '$filter', 'amMoment', '$rootScope', function($scope, $stateParams, units, $http, $filter, amMoment, $rootScope){
	$scope.units = units;
	$scope.unit = units.filter(function(unit){
			return unit.url_name == $stateParams.unit;
	})[0];

	$scope.next_unit = units.filter(function(unit, index, array){
		if(index == 0)
			return $scope.unit.id == array[array.length - 1].id;
		else
			return array[index - 1].id == $scope.unit.id;
	})[0];
	$scope.prev_unit = units.filter(function(unit, index, array){
		if(index == array.length - 1)
			return $scope.unit.id == array[0].id;
		else
			return array[index + 1].id == $scope.unit.id;
	})[0];


	$scope.dates = {};
	var today = moment();
	$scope.dates[today.format('YYYY-MM-DD')] = {
		date: today
	};
	$scope.dates[moment(today).add(1, 'days').format('YYYY-MM-DD')] = {
		date: moment(today).add(1, 'days')
	};
	$scope.dates[moment(today).add(2, 'days').format('YYYY-MM-DD')] = {
		date: moment(today).add(2, 'days')
	};


	$http.get('/schedule/'+ $scope.unit.id)
		.success(function(data){
			var dates = data.dates;
			for(var key in dates){
				if($scope.dates.hasOwnProperty(key))
					$scope.dates[key].value = dates[key].value;
			}
		})
		.error(function(error){
			$rootScope.showAlert(error.msg);
		});


	$scope.updateUnit = function(key){
		$scope.dates[key].processing = true;
		$http.put('/schedule/'+ $scope.unit.id, {
			date: key,
			on_off: $scope.dates[key].value ? 0 : 1
		})
			.success(function(data){
				$scope.dates[key].processing = false;
				$scope.dates[key].value = $scope.dates[key].value ? 0 : 1;
			})
			.error(function(error){
				console.log(error);
				$scope.dates[key].processing = false;
				$rootScope.showAlert(error.msg);
			});
	}

	$scope.temp = [];
	$scope.extremes = [];
	$scope.evapo = [];

	$http.get('/graphs/weather/'+ $scope.unit.id)
		.success(function(data){
			for(var i=0, l=data.extremes.length; i < l; i++)
				for(ii=0, ll=data.extremes[i].length; ii < ll; ii++)
					data.extremes[i][ii] = parseFloat(data.extremes[i][ii]) || null;

			for(var i=0, l=data.temp.length; i < l; i++)
				for(ii=0, ll=data.temp[i].length; ii < ll; ii++)
					data.temp[i][ii] = parseFloat(data.temp[i][ii]);

			for(var i=0, l=data.evapo.length; i < l; i++)
				for(ii=0, ll=data.evapo[i].length; ii < ll; ii++)
					data.evapo[i][ii] = parseFloat(data.evapo[i][ii]);

			for(var i=0, l=data.future_evapo.length; i < l; i++)
				for(ii=0, ll=data.future_evapo[i].length; ii < ll; ii++)
					data.future_evapo[i][ii] = parseFloat(data.future_evapo[i][ii]);

			$scope.weatherChartConfig.series[0].data = data.temp;
			$scope.weatherChartConfig.series[1].data = data.extremes;
			$scope.weatherChartConfig.series[2].data = data.evapo;
			$scope.weatherChartConfig.series[3].data = data.future_evapo;
			$scope.weatherChartConfig.loading = false;
		})
		.error(function(error){
			$rootScope.showAlert(error.msg);
		});


	Highcharts.setOptions({
		global: {
			useUTC : false,
		}
	});

	$scope.weatherChartConfig = {
		loading: true,
		options: {
			chart: {
				animation: false,
				height: 300,
				spacingBottom: 0,
			},
			tooltip: {
				crosshairs: true,
				shared: true,
			},
		},
		title: {
            text: null
        },
        xAxis: {
			minPadding:0,
			maxPadding:0,
            type: 'datetime',
			labels: {
				format: '{value:%m/%d}'
			},
			plotBands: [{ // mark the weekend
                color: '#f7f7f7',
                from: moment().startOf('day').toDate(),
                to: moment().add(5, 'day').startOf('day').toDate()
            }],
        },
        yAxis: [{
			max: 115,
			min: 40,
			tickAmount: 5,
			// alignTicks: false,
			// tickInterval: 60,
            title: {
                text: null
            },
			labels: {
				format: '{value}°F'
			},
			startOnTick: true,
			endOnTick: true,
        },
		{
			// min: 0,
			max: .5,
			tickAmount: 5,
			// alignTicks: false,
			// tickInterval: 60,
			opposite: true,
            title: {
                text: null
            },
			labels: {
				format: '{value}in'
			},
			startOnTick: true,
			endOnTick: true
		}],
        series: [{
            name: 'Avg Temperature',
            data: $scope.temp,
            zIndex: 1,
            marker: {
                fillColor: 'white',
                lineWidth: 2,
                lineColor: Highcharts.getOptions().colors[0]
            },
			yAxis: 0,
			tooltip: {
                valueSuffix: '°F'
            }
        }, {
            name: 'Temp Range',
			data: $scope.extremes,
            type: 'arearange',
            lineWidth: 0,
            linkedTo: ':previous',
            color: Highcharts.getOptions().colors[0],
            fillOpacity: 0.3,
            zIndex: 0,
			yAxis: 0,
			tooltip: {
                valueSuffix: '°F'
            }
        },
		{
            name: 'Evapotranspiration',
            data: $scope.evapo,
            zIndex: 1,
            marker: {
                fillColor: 'white',
                lineWidth: 1,
                lineColor: Highcharts.getOptions().colors[1]
            },
			yAxis: 1,
			tooltip: {
                valueSuffix: 'in'
            }
		},
		{
            name: 'ET Range',
			data: $scope.future_evapo,
            type: 'arearange',
            lineWidth: 0,
            linkedTo: ':previous',
            color: Highcharts.getOptions().colors[1],
            fillOpacity: 0.3,
            zIndex: 0,
			yAxis: 1,
			tooltip: {
                valueSuffix: 'in'
            }
		}]
	};



	$http.get('/graphs/hand/'+ $scope.unit.id)
		.success(function(data){
			for(var key in data.users)
			{
				for(var i=0, l=data.users[key].data.length; i<l; i++){
					data.users[key].data[i][0] = parseFloat(data.users[key].data[i][0]);
				}
				var settings = {
					animation: false,
					name: key,
					data: data.users[key].data,
					zIndex: 1,
					color: Highcharts.getOptions().colors[data.users[key].color]
				}

				$scope.handChartConfig.series.push(settings);
			}

			$scope.handChartConfig.loading = false;
		})
		.error(function(error){
			$rootScope.showAlert(error.msg);
		});



	$scope.handChartConfig = {
		loading: true,
		options: {
			chart: {
				animation: false,
				type: 'column',
				height: '200',
				spacingBottom: 0,
			},
			tooltip: {
				crosshairs: true,
				shared: true,
			},
		},
		title: {
            text: null
        },
        xAxis: {
			min: moment().subtract(20, 'days').format('x'),
			max: moment().startOf('day').format('x'),
			minPadding:0,
			maxPadding:0,
            type: 'datetime',
			labels: {
				format: '{value:%m/%d}'
			}
        },
        yAxis: {
            title: {
                text: null
            },
			startOnTick: true,
			endOnTick: true,
			min: 0,
			max: 10,
			tickAmount:6 
        },
        series: []
	};


	

	$http.get('/graphs/irrigation/'+ $scope.unit.id)
		.success(function(data){
			for(var i=0, l=data.data.length; i<l; i++){
				data.data[i][0] = parseFloat(data.data[i][0]);
			}
			var settings = {
				showInLegend: false,
				animation: false,
				name: 'Irrigation',
				data: data.data,
				zIndex: 1,
				// color: Highcharts.getOptions().colors[data.users[key].color]
			}

			$scope.irrigationChartConfig.series.push(settings);

			$scope.irrigationChartConfig.loading = false;
		})
		.error(function(error){
			$rootScope.showAlert(error.msg);
		});



	$scope.irrigationChartConfig = {
		loading: true,
		options: {
			chart: {
				animation: false,
				type: 'column',
				height: '100',
				spacingBottom: 0,
			},
			tooltip: {
				crosshairs: true,
				shared: true,
                valueSuffix: '%'
			},
		},
		title: {
            text: null
        },
        xAxis: {
			min: moment().subtract(21, 'days').format('x'),
			max: moment().startOf('day').format('x'),
			minPadding:0,
			maxPadding:0,
            type: 'datetime',
			labels: {
				format: '{value:%m/%d}'
			}
        },
        yAxis: {
			labels: {
				format: '{value}%'
			},
            title: {
                text: null
            },
			startOnTick: true,
			endOnTick: true,
			min: 0,
			max: 100,
			tickAmount:3
        },
        series: []
	};




	$http.get('/logs/'+ $scope.unit.id)
		.success(function(data){
			$scope.logs = data.logs;
		})
		.error(function(error){
			$rootScope.showAlert(error.msg);
		});



}])

.controller('reviewController', ['$scope', 'units', '$http', '$rootScope', function($scope, units, $http, $rootScope){
	$scope.units = units;
	$scope.dates = {};
	$scope.approved = {};

	var today = moment();

	$scope.dates[today.format('YYYY-MM-DD')] = {
		date: today
	};
	$scope.dates[moment(today).add(1, 'days').format('YYYY-MM-DD')] = {
		date: moment(today).add(1, 'days')
	};
	$scope.dates[moment(today).add(2, 'days').format('YYYY-MM-DD')] = {
		date: moment(today).add(2, 'days')
	};

	$http.get('/schedule')
		.success(function(data){
			$scope.water = data.units;
			if(data.approval)
			{
				$scope.approved = {
					timestamp: moment(data.approval.timestamp),
					user: data.approval.name,
				}
			}
		})
		.error(function(data){
			console.log(data);
			$rootScope.showAlert(data.msg, {type: 'warning', title: 'Error'});
		});

	$scope.approve = function(){
		$scope.approved.processing = true;
		$http.put('/approve_irrigation')
			.success(function(data){
				// $scope.approved.processing = false;
				$rootScope.showAlert(data.msg, {type: 'success', title: 'Error'});
				$scope.approved = {
					timestamp: moment(data.approval.timestamp),
					user: data.approval.name,
				}
				// console.log(data);
			})
			.error(function(data){
				$rootScope.showAlert(data.msg, {type: 'danger', title: 'Error'});
				console.log(data);
			});
	}
}])

.controller('irrigationController', ['$scope', '$http', 'units', '$rootScope', '$filter', function($scope, $http, units, $rootScope, $filter){
	$scope.units = units;
	$scope.dt = new Date();
	$scope.max_date = new Date();
	$scope.originalValues;

	$scope.submit = function(){
		// if(Object.keys($scope.radioModel).length < 1)
		if(angular.equals($scope.radioModel, $scope.originalValues))
			$rootScope.showAlert('Values are not different', {type: 'warning', title: 'Error'});
		else
		{
			$http.put('/irrigation', {
				units: $scope.radioModel,
				date: $filter('date')($scope.dt, 'yyyy-MM-dd')
			})
				.success(function(data){
					$rootScope.showAlert(data.msg, {type: 'success', title: 'Sucess'});
					$scope.originalValues = angular.copy($scope.radioModel);
				})
				.error(function(error){
					console.log(error);
					$rootScope.showAlert(data.msg, {type: 'danger', title: 'Error'});
				});
		}
	}

	$scope.updateDate = function(){
		var filtered_date = $filter('date')($scope.dt, 'yyyy-MM-dd');
		$http.get('/irrigation/'+ filtered_date, {date: 'today'})
			.success(function(data){
				$scope.originalValues = angular.copy(data.units);
				$scope.radioModel = data.units;
			})
			.error(function(error){
				$rootScope.showAlert(data.msg);
			});
	}

	$scope.$watch('dt', function(newDate){
		$scope.updateDate();
	});

}])

.controller('navigationController', ['$scope', 'units', function($scope, units) {
	$scope.units = units;
}])

.controller('addUserController', ['$scope', '$http', '$rootScope', function($scope, $http, $rootScope){
	$scope.submit = function(){
		// console.log('username: '+ $scope.data.user, 'password: '+ $scope.data.password);
		$http.put('/new_user', $scope.data)
			.success(function(data){
				$scope.data = {
					user: '',
					name: '',
					password: ''
				};
				$rootScope.showAlert(data.msg, {type: 'success'});
			})
			.error(function(data){
				$rootScope.showAlert(data.msg, {type: 'danger'});
			});
	}
}])

.controller('unitOrderController', ['$scope', '$http', '$rootScope', 'units', function($scope, $http, $rootScope, units){
	$scope.items = units;
	$scope.update = function(){
		var order = [];
		for(var i=0, l=units.length; i<l; i++){
			order.push(units[i].id);
		}
	}
}]);
