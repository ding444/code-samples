var Nightmare = require('nightmare');
var trim = require('trim');
var csv = require('csv');
var parse = require('csv-parse');
var moment = require('moment');
var mysql = require('mysql');
var settings = require('./settings.js');
var async = require('async');


var connection = mysql.createConnection({
	// debug: true,
	host		: settings.mysql.host || 'localhost',
	user		: settings.mysql.user || 'root',
	password	: settings.mysql.password || '',
	database	: settings.mysql.database
});

// Monkey patch for https://github.com/segmentio/nightmare/issues/126
// ( function() {
//     var sockjs = require( 'nightmare/node_modules/phantom/node_modules/shoe/node_modules/sockjs' );
//     if ( ! sockjs._createServerOld ) {
//         sockjs._createServerOld = sockjs.createServer;
//         sockjs.createServer = function( options ) {
//             if ( ! options ) {
//                 options = { };
//             }
//             if ( ! ( 'heartbeat_delay' in options ) ) {
//                 options.heartbeat_delay = 200;
//             }
//             return sockjs._createServerOld( options );
//         };
//     }
// } )();


// Misc variables

var today = moment().format('MMM DD, YYYY');
var yesterday = moment().subtract(8, 'days').format('MMM DD, YYYY');
var yesterday = moment().subtract(24, 'days').format('MMM DD, YYYY');


new Nightmare()
	.goto('http://weather.wsu.edu/?p=90550')
		.type('input[name=user]', 'ding444')
		.type('input[name=pass]', 'gomer1')
		.click('input[value=Login]')
		.wait()
		//.goto('http://weather.wsu.edu/awn.php?page=dailydata')
		.goto('http://weather.wsu.edu/index.php?p=93050')
		// .goto('http://weather.wsu.edu/awn.php?page=hourlydata')
		.wait()
		.select('#stationList', '330121')
		.wait()
		.select('#stationList', '330041')
		.wait()
		.type('#datepickerstart', yesterday)
		.type('#datepickerend', today)
		.click('#MyForm input[value=Submit]')
		.wait()
		.evaluate(function(){
			// return $('body').html();
			var xhr = new XMLHttpRequest();
			xhr.open('GET', '/utils/CsvOutput.php', false);
			xhr.send();
			return xhr.responseText.trim();
		}, function(csvBody){
			var royal_start = csvBody.indexOf('Royal City East, Grant County');

			var mattawa_csv = csvBody.substr(0, royal_start);
			mattawa_csv = trim(mattawa_csv.substr(0, mattawa_csv.lastIndexOf('\n')));
			var royal_csv = trim(csvBody.substr(royal_start));

			royal_csv = royal_csv.split('\n');
			royal_csv.splice(0,2);
			royal_csv = royal_csv.join('\n');

			mattawa_csv = mattawa_csv.split('\n');
			mattawa_csv.splice(0,2);
			mattawa_csv = mattawa_csv.join('\n');


			var column_names = [
				'date',
				'min_temp',
				'air_temp',
				'max_temp',
				'dew_point',
				'humidity',
				'leaf_wetness',
				'wind_dir',
				'wind_speed',
				'wind_max',
				'soil_2_in_temp',
				'soil_8_in_temp',
				'total_precip',
				'solar_radiation',
				'evapotranspiration_grass',
				'evapotranspiration_alfalfa'
			];

			async.parallel([function(next){
				parse(mattawa_csv, {columns: column_names}, function(err, output){
					output.shift();
					if(err) console.log(err)
					else
					{
						async.each(output, function(item, callback){
							delete item.undefined;
							connection.query('INSERT IGNORE INTO mattawa_daily_weather SET ?', item, function(err, results){
								if(err) console.log(err);
								callback();
							});
						}, function(err){
							next();
						});
					}
				});
			},
			function(next){
				parse(royal_csv, {columns: column_names}, function(err, output){
					output.shift();
					if(err) console.log(err)
					else
					{
						async.each(output, function(item, callback){
							delete item.undefined;
							connection.query('INSERT IGNORE INTO royal_daily_weather SET ?', item, function(err, results){
								if(err) console.log(err);
								callback();
							});
						}, function(err){
							next();
						});
					}
				});
			}],
			function(err, results){
				connection.end();
			});


		})
		.run(function(err, nightmare){
			if(err) console.log(err);
		});

