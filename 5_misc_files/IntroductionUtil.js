/*****************************************
 * Utility for handling first-time
 * conversations
 *****************************************/

const async = require('async');
const phrases = require('./PhraseUtil.js');
const request = require('request');

class IntroductionUtil {
	constructor() {
	}

	/*****************************************
	 * Check if info in db
	 *****************************************/
	check(user, db) {
		const collection = db.collection('users');
		collection.findOne({slack_user: user}, (error, results) => {
			if(error) console.error(error);
			console.log(results);
			if(!results)
			{
				return false;
			}
			else
			{
				return {
					in_db: false,
					name: message.user,
					gender: null,
					position: null,
				};
			}
		});
	}	


	/*****************************************
	 * Perform introduction
	 *****************************************/
	introduce(message, bot, db) {
		let data = {
			slack_user: message.user	
		};

		bot.startPrivateConversation(message, (response, convo) => {
			bot.api.users.info({user: message.user}, (error, response) => {
				if(error) logger.log('error', 'Invalid API lookup', error, response);
				let {user, profile: {first_name, last_name}, real_name} = response.user;

				async.waterfall([
					(callback) => {
						convo.ask(`According to Slack your name is ${first_name}.  Is this correct?`, [
							{
								pattern: bot.utterances.yes,
								callback: (response, convo) => {
									data.name = first_name;
									callback(null, convo);
								}
							},
							{
								default: true,
								callback: (response, convo) => {
									convo.ask('What should I call you then?', (response, convo) => {
										data.name = response.text;
										convo.say(phrases.get_random_phrase('ok'));
										convo.next();
										callback(null, convo);
									});
									convo.next();
								}
							},
						]);
						convo.next();
					},
					(convo, callback) => {
						request({
							method: 'GET',
							uri: 'https://api.genderize.io',
							qs: {name: data.name},
							json: true,
						}, (err, response, body) => {
							console.log(body);
							let {gender, probability, count} = body;
							gender = gender || 'male';
							convo.ask(`I called Miss Cleo and she says she feels like you're a ${gender}.  Is she correct?\n(http://cdn.deanc.me/hRAd) `, [
								{	
									pattern: bot.utterances.yes,
									callback: (response, convo) => {
										convo.say('Spooky');
										data.gender = gender;
										convo.next();
										callback(null, convo);
									}
								},
								{
									default: true,
									callback: (response, convo) => {
										convo.say('I never believed in psychics anyway');
										data.gender = (gender == 'male') ? 'female' : 'male';
										convo.next();
										callback(null, convo);

									}
								}
							]);
							convo.next();
						});
					},
					(convo, callback) => {
						convo.ask('What is your position within the company?', (response, convo) => {
							data.position = response.text;
							convo.next();
							callback(null, convo);
						});
					},
				],(err, convo) => {
					if(err) console.error(err);	
					let msg = `Here are all of the things I've learned about you\n>>>`;
					msg += `• Your name is ${data.name}\n`;
					msg += `• You are ${data.gender}\n`;
					msg += `• Your position is ${data.position}\n`;
					convo.say(msg);
					convo.ask('Does all of this look correct?', [
						{
							pattern: bot.utterances.yes,
							callback: (response, convo) => {
								convo.say('Perfect.  I will remember this for later');
								convo.next();

								//STORE IN MONGO
								const collection = db.collection('users');
								collection.remove({slack_user: message.user}, (error, result) => {
									if(error) console.error(error);
									collection.insert(data, (error, records) => {
										if(error) console.error(error);
										console.log(records);
									});
								});
							}
						},
						{
							default: true,
							callback: (response, convo) => {
								convo.say(`Bummer.  I wonder where this went wrong.  Let's try again.`);
								convo.next();
								this.introduce(message, bot);
							}
						}
					])
				});
			});
		});
	}

	// DISC
	do_disc(bot, message, db) {
		const questions = [
			// question 1
			{
				keys: {
					best: ['s', 'i', 'c', 'x'],
					least: ['s', 'x', 'c', 'd'],
				},
				answers: [
					'Consideration towards others',
					'Enthusiastic acting',
					'Demanding self-expectations',
					'Does things in new ways',
				],
			},
			// question 2
			{
				keys: {
					best: ['i', 'c', 'd', 'x'],
					least: ['i', 'c', 'd', 's'],
				},
				answers: [
					'Appeals to others',
					'Thinks things over',
					'Holds their ground',
					'Pleasant to others',
				],
			},
			// question 3
			{
				keys: {
					best: ['x', 'd', 's', 'i'],
					least: ['c', 'd', 'x', 'i'],
				},
				answers: [
					'Knows what others want',
					'Daring to take risks',
					'Responsive to others',
					'Attracts others',
				],
			},
			// question 4
			{
				keys: {
					best: ['i', 'c', 'x', 's'],
					least: ['x', 'c', 'd', 's'],
				},
				answers: [
					'Receptive to new ideas',
					'Avoids conflict',
					'Sticks to decisions',
					'Steady-paced acting',
				],
			},
			// question 5
			{
				keys: {
					best: ['x', 'c', 'x', 's'],
					least: ['i', 'c', 'd', 's'],
				},
				answers: [
					'Animated, playful',
					'Doing things accurately',
					'Bold-acting',
					'Not easily-angered',
				],
			},
			// question 6
			{
				keys: {
					best: ['d', 's', 'x', 'x'],
					least: ['d', 's', 'i', 'c'],
				},
				answers: [
					'Wants to win more',
					'Thoughtful of others',
					'Carefree-acting',
					'Cautious with others',
				],
			},
			// question 7
			{
				keys: {
					best: ['x', 's', 'd', 'i'],
					least: ['c', 'x', 'd', 'i'],
				},
				answers: [
					'Wanting things exact',
					'Will do as told',
					'Driven to be first',
					'Makes things enjoyable',
				],
			},
			// question 8
			{
				keys: {
					best: ['d', 'i', 'x', 'x'],
					least: ['x', 'x', 's', 'c'],
				},
				answers: [
					'Tough, fearless-acting',
					'Seeks to persuade others',
					'Does not disagree',
					'Questioning, careful-acting',
				],
			},
			// question 9
			{
				keys: {
					best: ['i', 's', 'd', 'c'],
					least: ['i', 's', 'd', 'x'],
				},
				answers: [
					'Warm towards others',
					'Patient, remains calm',
					`Doesn't depend on others`,
					'Acts quietly, calmly',
				],
			},
			// question 10
			{
				keys: {
					best: ['d', 'c', 'x', 'c'],
					least: ['d', 'x', 'i', 's'],
				},
				answers: [
					'Looks for new ideas',
					'Active, scanning mind',
					'Moves towards others',
					'Moderate-acting',
				],
			},
			// question 11
			{
				keys: {
					best: ['i', 's', 'x', 'd'],
					least: ['i', 's', 'c', 'd'],
				},
				answers: [
					'Likes to talk',
					'Slow to share feelings',
					'Proven, clear-cut actions',
					'Quick decision-maker',
				],
			},
			// question 12
			{
				keys: {
					best: ['x', 'd', 'c', 's'],
					least: ['i', 'd', 'x', 's'],
				},
				answers: [
					'Seeks favourable conditions',
					'Willing to take risks',
					'Assesses people',
					'Easily-satisfied',
				],
			},
			// question 13
			{
				keys: {
					best: ['d', 'i', 's', 'x'],
					least: ['x', 'i', 's', 'c'],
				},
				answers: [
					'Acts in forceful way',
					'Outgoing actions',
					'Gets on well with others',
					'Calculates risks',
				],
			},
			// question 14
			{
				keys: {
					best: ['c', 'd', 'i', 's'],
					least: ['c', 'x', 'i', 'x'],
				},
				answers: [
					'Plans their actions',
					'Doing it your own way',
					'Promoting agreement',
					'Friendly, low-keyed',
				],
			},
			// question 15
			{
				keys: {
					best: ['s', 'd', 'c', 'x'],
					least: ['x', 'd', 'c', 'i'],
				},
				answers: [
					'Likely to help others',
					'Driven towards action',
					'Diplomatic with others',
					'Strong personal feelings',
				],
			},
			// question 16
			{
				keys: {
					best: ['i', 'x', 'x', 'd'],
					least: ['x', 's', 'c', 'd'],
				},
				answers: [
					'Tends to be trusting',
					'Tries to understand others',
					'Identifies others’ intentions',
					'Confidently forceful',
				],
			},
			// question 17
			{
				keys: {
					best: ['c', 's', 'x', 'd'],
					least: ['x', 's', 'i', 'd'],
				},
				answers: [
					'Wants to know expectations',
					'Not selfish',
					'Lively and active',
					'Refuses to give-up easily',
				],
			},
			// question 18
			{
				keys: {
					best: ['i', 's', 'x', 'd'],
					least: ['x', 'x', 'c', 'd'],
				},
				answers: [
					'Worthy of being included',
					'Comfortable working with others',
					'Easily-frustrated',
					'Strong personal views',
				],
			},
			// question 19
			{
				keys: {
					best: ['c', 'd', 'i', 's'],
					least: ['x', 'd', 'i', 's'],
				},
				answers: [
					'Displays intense attention',
					'Seeks new results',
					'Tends to be optimistic',
					'Willing to defer to others',
				],
			},
			// question 20
			{
				keys: {
					best: ['d', 'c', 'x', 'i'],
					least: ['d', 'x', 's', 'i'],
				},
				answers: [
					'May argue with others',
					'Avoids difficulties',
					'May be nonchalant',
					'Spontaneous-acting',
				],
			},
			// question 21
			{
				keys: {
					best: ['i', 'x', 'd', 'c'],
					least: ['i', 's', 'd', 'c'],
				},
				answers: [
					'Seeks others’ views',
					'Satisfied by same results',
					'Self assured views',
					'Tends to be worrisome',
				],
			},
			// question 22
			{
				keys: {
					best: ['i', 'x', 'd', 's'],
					least: ['i', 'c', 'd', 's'],
				},
				answers: [
					'Likes to be with others',
					'Correct, precise actions',
					'Powerful-acting',
					'Easy on others',
				],
			},
			// question 23
			{
				keys: {
					best: ['i', 'c', 'd', 'x'],
					least: ['i', 'x', 'c', 's'],
				},
				answers: [
					'Makes others comfortable',
					'Analyses task, conditions',
					'Speaks openly, directly',
					'Reserved and receptive',
				],
			},
			// question 24
			{
				keys: {
					best: ['d', 's', 'i', 'c'],
					least: ['d', 's', 'i', 'c'],
				},
				answers: [
					'Seeks change to improve results',
					'Finds people interesting',
					'Wants to be liked by others',
					'Strong, private thoughts',
				],
			},
		];

		const thresholds = {
			best: {
				d: [2, 5, 7, 9, 12],
				i: [2, 3, 5, 6, 7],
				s: [2, 3, 5, 7, 10],
				c: [1, 3, 4, 5, 7],
			},
			least: {
				d: [12, 8, 4, 2, 1],
				i: [7, 5, 3, 2, 1],
				s: [10, 7, 5, 4, 3],
				c: [10, 8, 6, 4, 3],
			},
		};

		const questions1 = [
			{
				keys: {
					best: ['s', 'i', 'c', 'x'],
					least: ['s', 'x', 'c', 'd'],
				},
				answers: [
					'Consideration towards others',
					'Enthusiastic acting',
					'Demanding self-expectations',
					'Does things in new ways',
				],
			},
		];

		const reports = {
			d: {
				name: 'Dominance',
				desc: 'emphasis is on shaping the environment by overcoming opposition to accomplish results',
				tendencies: [
					'getting immediate results',
					'causing action',
					'accepting challenges',
					'making quick decisions',
					'questioning the status quo',
					'taking authority',
					'managing trouble',
					'solving problems',
				],
				environment: [
					'power and authority, prestige and challenge',
					'opportunity for individual accomplishments',
					'wide scope of operations',
					'direct answers',
					'opportunity for advancement',
					'freedom from controls and supervision',
					'many new and varied activities',
				],
				action_plan: [
					'weigh pros and cons',
					'calculate risks',
					'use caution',
					'structure a more predictable',
					'environment',
					'research facts',
					'deliberate before deciding',
					'recognise the needs of others',
				],
				needs: [
					'difficult assignment',
					'understanding that they need people',
					'techniques based on practical experience',
					'an occasional shock',
					'identification with a group',
					'to voice the reasons for conclusions',
					'an awareness of existing sanctions',
					'to pace self and to relax more',
				],
			},
			i: {
				name: 'Influencing of Others',
				desc: 'emphasis is on shaping the environment by bringing others into alliance to accomplish results',
				tendencies: [
					'contacting people',
					'making a favourable impression',
					'expressing themselves fluently',
					'creating a motivational environment',
					'generating enthusiasm',
					'entertaining people',
					'desiring to help others',
					'participating in a group',
				],
				environment: [
					'popularity, social recognition, public recognition of ability',
					'freedom of expression',
					'group activities outside of the job',
					'democratic relationships',
					'freedom from control and detail',
					'opportunity to talk about proposals',
					'coaching and counselling skills',
					'favourable working conditions',
				],
				action_plan: [
					'concentrate on the task',
					'seeks facts',
					'speak directly',
					'respect sincerity',
					'develop systematic approaches',
					'prefer dealing with things to dealing with people',
					'take a logical approach',
					'demonstrate individual followthrough',
				],
				needs: [
					'control of time, if D or S is below the midline',
					'objectivity in decision-making',
				],
			},
			s: {
				name: 'Steadiness',
				desc: 'emphasis is on co-operating with other to carry out the task',
				tendencies: [
					'performing an accepted work pattern',
					'sitting or staying in one place',
					'demonstrating patience',
					'developing specialised skills',
					'concentrating on the task',
					'showing loyalty',
					'being a good listener',
					'calming excited people',
				],
				environment: [
					'security of the situation',
					'status quo unless given reasons for change',
					'minimal work infringement on home life',
					'credit for work accomplished',
					'limited territory',
					'sincere appreciation',
					'identification with a group',
					'traditional procedures',
				],
				action_plan: [
					'react quickly to unexpected change   stretch toward the challenges of an accepted task',
					'become involved in more than one thing',
					'are self-promoting',
					'apply pressure on others',
					'work comfortably in an unpredictable environment',
					'delegate to others',
					'are flexible in work procedures',
					'can contribute to the work',
				],
				needs: [
					'conditioning prior to change',
					'validation of self-worth',
					'information on how one\'s efforts',
					'contribute to the total effort',
					'work associates of similar competence and sincerity',
					'guidelines for accomplishing the task',
					'encouragement of creativity',
					'confidence in the ability of others',
				],
			},
			c: {
				name: 'Conscientious',
				desc: 'emphasis is on working with existing circumstances to promote quality in products or service',
				tendencies: [
					'attention to key directives and standards',
					'concentrating on key details',
					'working under known circumstances',
					'being diplomatic with people',
					'checking for accuracy',
					'critical thinking',
					'critical of performance',
					'complying with authority',
				],
				environment: [
					'security assurances',
					'standard operating procedures',
					'sheltered environment',
					'reassurance',
					'no sudden or abrupt changes',
					'being part of a work group',
					'personal responsiveness to their effort',
					'status quo unless assured of quality control',
					'door openers who call attention to accomplishments',
				],
				action_plan: [
					'desire to expand authority',
					'delegate important tasks,',
					'make quick decisions',
					'use policies only as guidelines',
					'compromise with the opposition',
					'state unpopular positions',
				],
				needs: [
					'precision work',
					'opportunity for careful planning',
					'exact job and objective descriptions',
					'scheduled performance appraisals',
					'as much respect for people\'s personal worth as for what they accomplish',
					'to develop tolerance for conflict',
				],
			},
		};

		bot.reply(message, `Let's do a DISC profile assessment.  I will ask you a series of 48 questions to which you must choose *one* answer out of four provided answers. The test should take approximately 15 minutes.  To cancel the test and come back to it later type \`cancel\``);
		bot.startConversation(message, (err, convo) => {
			let results = {
				best: {d: 0, i: 0, s: 0, c: 0, x: 0},
				least: {d: 0, i: 0, s: 0, c: 0, x: 0},
			};

			// Ask each question
			async.eachOfSeries(questions, (question, key, callback) => {
				// Create string for answers in question
				let is_first = true;
				let first_answer = undefined;


				// Ask each question twice:
				// Once for most alike and least alike
				async.eachOfSeries(['best', 'least'], (item, key2, callback2) => {
					// Errors for offering help
					let err_count = 0;
					let answers_str = '';
					question.answers.forEach((answer, qkey) => {
						const answer_num = qkey + 1;
						answers_str += `${first_answer == answer_num ? '~' : ''}${answer_num}. ${answer}${first_answer == answer_num ? '~' : ''}\n`;
					});
				
					// Ask question
					convo.ask(`*Question ${(key * 2)+(key2 + 1)}*:\n_Which of these statements *${item}* describes you?_\n>>>\n${answers_str}`, (response, convo) => {
						const reply = parseInt(response.text.trim());

						// If response is 1-4 generate result
						if(reply > 0 && reply < 5 && reply !== first_answer)
						{
							const answers = question.keys[item];
							const letter = answers[reply - 1];

							// incremement letter in results object
							results[item][letter]++;
							first_answer = reply;
							console.log('first-answer', first_answer);
							callback2();
						}
						// Invalid input
						else
						{
							err_count++;

							if(reply == first_answer)
							{
								bot.reply(message, '_You cannot select the same answer for what *best* describes you and what *least* describes you._');
							}
							bot.reply(message, '_Please select a numeric answer from 1 to 4_');

							// repeat question if error 3 times or more
							if(err_count > 2)
							{
								convo.repeat();
								convo.next();
							}
							else
							{
								// don't repeat
								// just wait for new answer
								convo.silentRepeat();
							}
						}
					});
					convo.next();
				},
				//done
				(err) => {
					// finished with both versions of question: next()
					callback();
				});
			},
			// End
			(err) => {
				// Finished with questions. echo results
				// create array for sorted values
				let sortable = {
					best: [],
					least: [],
				};

				// Loop through each result and add to sortable array
				for(let key in results) {
					for(let key2 in results[key])
						sortable[key].push([key2, results[key][key2]]);
				}
				
				// Sort values
				for(let key in results) {
					sortable[key].sort((a,b) => {
						return b[1] - a[1];
					});
				}

				let new_results = {
					best: {d:1,i:1,s:1,c:1},
					least: {d:1,i:1,s:1,c:1},
				};

				for(let key in results) {
					sortable[key].forEach((item, key2) => {
						const [category, value] = item;
						
						if(category != 'x')
						{
							const thresh_arr = thresholds[key][category];

							thresh_arr.forEach((thresh_val, thresh_key) => {
								if(key == 'best' && value >= thresh_val)
								{
									new_results[key][category] = (thresh_key + 2);
								}
								else if(key == 'least' && value <= thresh_val)
								{
									new_results[key][category] = (thresh_key + 2);
								}
							});
						}
					});
				}

				let spaces = 12;
				let data_table = `
|  D  |  I  |  S  |  C  |${' '.repeat(spaces)}|  D  |  I  |  S  |  C  |
=========================${' '.repeat(spaces)}=========================
|  Expected by Others   |${' '.repeat(spaces)}| Response to pressure  |
=========================${' '.repeat(spaces)}=========================
`;
				for(let i=6, l=1; i>= l; i--)
				{
					['d', 'i', 's', 'c'].forEach((letter) => {
						data_table += `|  ${new_results.best[letter] == i ? '█' : ' '}  `;
					});

					data_table += '|';
					data_table += ' '.repeat(spaces);

					['d', 'i', 's', 'c'].forEach((letter) => {
						data_table += `|  ${new_results.least[letter] == i ? '█' : ' '}  `;
					});

					data_table += '|\n';
					const line_char = (i % 3) == 1 ? '=' : '-';
					data_table += line_char.repeat(25)
					data_table += ' '.repeat(spaces);
					data_table += line_char.repeat(25)
					data_table += '\n';
				}
				
				let highs = [];
				let high;

				['d', 'i', 's', 'c'].forEach((letter) => {
					if(!high || new_results.best[letter] > new_results.best[high])
					{
						high = letter;
					}

					if(new_results.best[letter] > 4)
					{
						highs.push(letter);
					}
				});

				let report = reports[high];
				convo.say('I know this looks ugly and I will eventually replace it with a beautiful looking graphic, but here are your results:\n```'+ data_table +'```');
				convo.say(`Based on your results you are a high *${report.name}*, which means your ${report.desc}.`);
				convo.say(`*Your tendencies include:*\n• ${report.tendencies.join('\n• ')}`);
				convo.say(`*You desire an environment which includes:*\n• ${report.environment.join('\n• ')}`);
				convo.say(`*You need others who:*\n• ${report.action_plan.join('\n• ')}`);
				convo.say(`*To be more effective, you need:*\n• ${report.needs.join('\n• ')}`);
				convo.next();
			});
		});
	};


	
}

module.exports = new IntroductionUtil();
