const express  = require('express'),
	bodyParser = require('body-parser'),
	mysql      = require('mysql');
	settings   = require('./settings.json');

let app = express();

let connection = mysql.createConnection({
	user     : settings.mysql.user,
	password : settings.mysql.password,
	database : settings.mysql.database
});

connection.connect();

// Middleware
app.use(express.static('public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Error handling
app.use(function(err, req, res, next) {
	console.error(err.stack);
	res.status(500).send('Something broke!');
});

// Routes
app.post('/signup', (req, res) => {
	let email_address = req.body.email;
	let email_regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

	// If email is set and is valid
	if(email_address && email_regex.test(email_address))
	{
		connection.query('INSERT INTO email_signups SET ?', {email_address: email_address}, (err, result) => {
			if(err)
			{
				console.log(err);
				res.json({status: 'error', error: 'Unable to add email address'});
			}
			else
				res.json({status: 'success', message: 'Thank you for signing up.<br>We\'ll be in touch.'});
		});
	}
	else
		res.json({status: 'error', error: 'Invalid email address'});
	
});

app.listen(process.env.PORT || 3000);
