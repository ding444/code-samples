SELECT
	z.customer_id AS `Customer ID`,
	z.customer_name AS `Customer Name`,
	z.service AS `Service`,
	ROUND(z.download/POWER(10,9),2) AS `Download (GB)`,
	ROUND(z.upload/POWER(10,9),2) AS `Upload (GB)`,
	ROUND(z.total/POWER(10,9),2) AS `Total (GB)`,
	z.cap/POWER(10,9) AS `Plan Cap (GB)`,
	CONCAT(ROUND(z.total / z.cap * 100, 2),'%') AS `Percent Used`
FROM(
	SELECT
		a.CustomerID AS `customer_id`,
		CONCAT(b.FirstName, ' ', b.LastName) AS `customer_name`,
		d.Discription AS `service`,
		SUM(a.BandwidthDown) AS `download`,
		SUM(a.BandwidthUp) AS `upload`,
		(SUM(a.BandwidthDown) + SUM(a.BandwidthUp)) AS `total`,
		e.MaxMegs * Power(10,6) AS `cap`
	FROM BandwidthHistory AS a

	LEFT JOIN Customer AS b
	ON a.CustomerID = b.CustomerID

	LEFT JOIN CustomerServices AS c
	ON b.customerID = c.customerID

	LEFT JOIN Services AS d
	ON c.ServiceID = d.ID

	LEFT JOIN InternetInfo AS e
	ON d.ID = e.ServiceID

	WHERE a.date > '2016-11-01' 
	AND a.date < '2016-11-30'
	AND d.type = 'Monthly Internet'

	GROUP BY a.CustomerID
	ORDER BY `total` DESC

	LIMIT 100
) AS z
