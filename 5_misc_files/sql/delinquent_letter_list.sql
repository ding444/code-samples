SELECT
	a.CustomerID AS `customer_id`,
	a.CompanyName AS `account_name`,
	a.Status as `status`,
	b.Address1 AS `address_1`,
	b.Address2 AS `address_2`,
	b.City AS city,
	b.State AS state,
	b.ZipCode AS zip,
	(IFNULL(c.credits,0) - IFNULL(d.debits,0)) AS balance
FROM Customer AS a
LEFT JOIN Address AS b
	ON a.CustomerID = b.CustomerID
LEFT JOIN (
	SELECT
		CustomerID,
		SUM(Amount) AS credits
	FROM Credits
	GROUP BY CustomerID
) AS c
	ON a.CustomerID = c.CustomerID
LEFT JOIN (
	SELECT
		CustomerID,
		SUM(Amount) AS debits
	FROM Debits
	GROUP BY CustomerID
) AS d
ON a.CustomerID = d.CustomerID
WHERE a.Status = '45 Day Delinquent' AND b.Type = 'Other'
