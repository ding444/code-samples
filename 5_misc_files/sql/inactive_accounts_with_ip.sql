SELECT
	b.CustomerID AS `Customer ID`,
	b.CompanyName AS `Company Name`,
	c.Name AS `Address Range`,
	INET_NTOA(a.IPAddress + c.StartAddress) AS `IP Address`
FROM Equipment AS a
LEFT JOIN Customer AS b
ON a.EndUserID = b.CustomerID
LEFT JOIN AddressRange AS c
ON a.IPType = c.AddressRangeID
WHERE b.Status = 'Not Active'

