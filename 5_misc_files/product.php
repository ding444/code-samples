<?php

class Controller_Product extends Controller_Template
{

	public function before()
	{
		if(Input::protocol() != 'https')
		{
			$url = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
			Response::redirect($url, 'location', 301);
			die;
		}

		parent::before();
	}
	
	public function action_tutorial()
	{
		$this->template->title = 'Vizzy Pop - Tutorial';
		$this->template->content = View::forge('product/tutorial');
	}

	public function action_add()
	{
		if(Input::post())
		{
			$config = array(
				'path' => APPPATH.DS.'uploads/tmp/',
				'randomize' => true,
				'ext_whitelist' => array('jpg', 'jpeg', 'gif', 'png'),
				'mime_whitelist' => array('image/jpeg', 'image/png', 'image/gif', 'image/tiff'),
			);
				

			Upload::process($config);

			$val = Validation::forge();
			$val->add('picture_name', 'Picture name')->add_rule('required')
				->add('num_walls', 'Number of walls')->add_rule('required')->add_rule('numeric_min', 1)->add_rule('numeric_max', 3);

			$num_walls = is_numeric(Input::post('num_walls')) ? Input::post('num_walls') : 0;
			
			$wall_count = 0;
			for($i=1;$i<=$num_walls;$i++)
			{
				for($n=1;$n<=5;$n++)
					$val->add('wall'. $i .'.'. $n, 'wall #'. $i .' color '. $n)->add_rule('required');
			}

			if ($val->run())
			{
				if (Upload::is_valid())
				{
					Upload::save(0);
					$file_info = Upload::get_files(0);

					$file_name = $file_info['saved_as'];
					$pic_name = Input::post('picture_name');
					$num_walls = Input::post('num_walls');
					$colors = array();

					for($i=1;$i<=$num_walls;$i++)
					{
						for($n=1;$n<=5;$n++)
							$colors[$i][$n] = Input::post('wall'. $i .'.'. $n);
					}

					$session_arr = array(
						'file' => $file_name,
						'name' => $pic_name,
						'num_walls' => $num_walls,
						'colors' => $colors
					);

					Session::set('cart.'. str_replace('.','_',$file_name), $session_arr);

					$checkout_btn = Input::post('finish', false);
					echo $checkout_btn;
					if($checkout_btn !== false)
						Response::redirect('product/checkout');
					else
						Response::redirect('product/add');
				}
				else
				{
					$file_errors = Upload::get_errors('picture_file');
					if(count($file_errors) > 0)
					{
						$data['inputs'] = $val->input();
						$data['errors'] = '<ul>';
						foreach($file_errors['errors'] as $item)
							$data['errors'] .= '<li>'. $item['message'].'</li>';

						$data['errors'] .= '</ul>';

					}
				}
			}
			else
			{
				$data['inputs'] = $val->input();
				$data['errors'] = $val->show_errors();
			}
		

		}
		else
		{
			$data = array(
				'inputs' => array(
					'picture_name'=>'',
					'num_walls'=>'',
				),
				'errors' => ''
			);
		}
		
		for($i=1;$i<=3;$i++)
		{
			for($n=1;$n<=5;$n++)
			if(empty($data['inputs']['wall'.$i.'.'.$n]))
				$data['inputs']['wall'.$i.'.'.$n] = '';
		}


		$this->template->title = 'Vizzy Pop - Add Product';
		$data['cart'] = $this->make_cart();
		$this->template->content = View::forge('product/add', $data, FALSE);
	}

	public function action_checkout()
	{

		Package::load('stripe');
		$stripe_keys = Config::load('stripe.php');
		Stripe::setApiKey($stripe_keys['secret_key']);
		
		if(Input::post())
		{
			$val = Validation::forge();
			
			$val->add('customer_name', 'your name')->add_rule('required')
				->add('email_address', 'your email address')->add_rule('required')->add_rule('valid_email');
			

			if ($val->run())
			{
					Session::set('order', array('customer_name'=>Input::post('customer_name'), 'email_address'=>Input::post('email_address'), 'coupon_code'=>Input::post('coupon_code')));
					Response::redirect('product/checkout_final');
			}
			else
			{
				$data['errors'] = $val->show_errors();
			}
		}
		else
		{
			$data['errors'] = '';
		}

		$data['payment']['amount'] = count(Session::get('cart')) * 2500;
		$data['payment']['description'] = 'Total: $'. count(Session::get('cart')) * 25 .' for '. count(Session::get('cart')) .' images';

		$data['cart'] = $this->make_cart();
		$data['public_key'] = $stripe_keys['public_key'];
		$this->template->title = 'Vizzy Pop - Checkout';
		$this->template->content = View::forge('product/checkout', $data, FALSE);
	}

	public function action_checkout_final()
	{
		Package::load('stripe');
		$stripe_keys = Config::load('stripe.php');
		Stripe::setApiKey($stripe_keys['secret_key']);

		$customer_info = Session::get('order');

		if(!empty($customer_info['coupon_code']))
		{
			$coupon = Model_Coupons::find_one_by('coupon_code', $customer_info['coupon_code']);
			if($coupon !== null)
			{
				if($coupon->expiration_date === null || time() < strtotime($coupon->expiration_date))
				{
					$discount = $coupon->discount;
				}
			}
		}
		
		if(Input::post())
		{
			$val = Validation::forge();
			
			$val->add('stripeToken', 'the payment token')->add_rule('required');

			if ($val->run() || $discount == 100)
			{
				if(!empty($discount) && $discount == 100)
					null;
				else
				{
				try
				{
					$charge = Stripe\Stripe_Charge::create(array(
						'amount'		=> count(Session::get('cart')) * 2500 * (!empty($discount) ? (100 - $discount) / 100 : 1),
						'currency'		=> 'usd',
						'card'			=> Input::post('stripeToken'),
						'description'	=> $customer_info['customer_name'] .' - '. $customer_info['email_address'],
					));
				} catch(\Stripe_CardError $e) {
					//	The card has been declined
					
				}
				}

				if((!empty($charge) && $charge['paid'] === true) || $discount == 100)
				{
					$transaction_id = !empty($charge['id']) ? $charge['id'] : 'free'.md5(time());
					$charge = !empty($charge) ? $charge : 'free';
					
					//move files to folder with transaction id
					File::create_dir(APPPATH.DS.'uploads'.DS.'orders', $transaction_id);
					$cart_items = Session::get('cart');
					foreach($cart_items as $item)
						File::copy(APPPATH.DS.'uploads'.DS.'tmp'.DS.$item['file'], APPPATH.DS.'uploads'.DS.'orders'.DS.$transaction_id.DS.$item['file']);
					var_dump(Session::get());
					$order = Model_Orders::forge(array(
					    'payment_id' => $transaction_id,
						'cart_items' => serialize(Session::get()),
						'charge' => serialize($charge),
						'name'	=> $customer_info['customer_name'],
						'email' => $customer_info['email_address'],
						'coupon_code' => $customer_info['coupon_code'],
					));
					$result = $order->save();
					$order_id = $result[0];
					//send order confirmation to customer
					\Package::load('email');
					$receipt_email = Email::forge();
					$receipt_email->from('orders@vizzypop.com', 'Vizzy Pop Order');
					$receipt_email->to($customer_info['email_address']);
					$receipt_email->subject('Your Vizzy Pop order');
					$message = 'Thank you for using Vizzy Pop.  This is a summary of your recent order'."\n\n";
					$message .= '---------------------------------------------'."\n";
					$message .= 'Order number: '. $order_id ."\n";
					$message .= 'Date: '. date('F j, Y') ."\n";
					$message .= 'Account: '. $customer_info['email_address'] ."\n";
					if(!empty($charge['id']))
						$message .= 'Paid: $'. (!empty($charge['amount']) ? number_format($charge['amount'] / 100, 2) : 'free') ."\n";
					
					$message .= "\n".'Ordered'."\n";
					foreach($cart_items as $item)
						$message .= "\t". $item['name'].', '. $item['num_walls'] . ' '. ($item['num_walls'] == 1 ? 'wall': 'walls')."\n";
						
					$message .= '---------------------------------------------'."\n\n";
					$message .= 'If you have any questions, please refer to your Order ID ('. $order_id.') whenever contacting us.'."\n\n";
					$message .= 'Thank you,'."\n\n";
					$message .= 'Vizzy Pop';
					$receipt_email->body($message);
					$receipt_email->send();

					//send order to owner
					$email = Email::forge();
					$email->from('orders@vizzypop.com', 'Vizzy Pop Order');
					$email->to(array(
						/*'anapaula@cocoa.am',
						'rodrigo@cocoa.am',
						'scott@hencil.com',*/
						'orders@vizzypop.com'
					));
					$email->bcc('dean@hourlywebdesign.com');
					$email->subject('New Vizzy Pop order');
					$message = 'A new order with the following info has been submitted:

';
					$message .= 'Purchaser: '. $customer_info['customer_name'] ."\n";
					$message .= 'Email: '. $customer_info['email_address'] ."\n";
					foreach($cart_items as $item)
					{
						$message .= 'Name: '. $item['name'] ."\n";
						$message .= 'Filename: '. $item['file'] ."\n";
						for($i=1;$i<4; $i++)
						{
							if(!empty($item['colors'][$i]) && is_array($item['colors'][$i]))
								$message .= 'Wall '. $i .': '. implode(' ,', $item['colors'][$i]) ."\n";
						}
						$message .= "\n";

						$new_name = $item['name'];
						$new_name .= substr($item['file'], strrpos($item['file'], '.'));
						$file_path = APPPATH.DS.'uploads'.DS.'orders'.DS.$transaction_id.DS.$item['file'];
						$email->attach($file_path, false, null, null, $new_name);
					}

					$email->body($message);
					$email->send();
					//display receipt page
					Session::destroy();
					Session::set_flash('order_id', $order_id);
					
					Response::redirect('product/view_order');
				}
				else
				{
					$data['errors'] = 'Payment was not successful';
				}
			}
			else
			{
				$data['errors'] = $val->show_errors();
			}
		}
		else
		{
			//Response::redirect('product/checkout');
			$data['errors'] = '';
			if(count(Session::get('cart')) < 1)
				Response::redirect('product/checkout');
		}
		
		$data['discount'] = !empty($discount) ? $discount : false;
		$data['cart_items'] = Session::get('cart');
		$data['payment']['amount'] = count(Session::get('cart')) * 2500 *(!empty($discount) ? (100 - $discount) / 100 : 1);
		$data['payment']['description'] = 'Total: $'. count(Session::get('cart')) * 25 *(!empty($discount) ? (100 - $discount) / 100 : 1) .' for '. count(Session::get('cart')) .' images';

		$data['cart'] = $this->make_cart();
		$data['public_key'] = $stripe_keys['public_key'];
		$this->template->title = 'Vizzy Pop - Checkout';
		$this->template->content = View::forge('product/checkout_final', $data, FALSE);
	}


	public function action_view_order($order_id=null)
	{
		Package::load('stripe');
		$order_id = $order_id ?: Session::get_flash('order_id', null, false);
		if(empty($order_id))
			throw new HttpNotFoundException;
		Session::keep_flash('order_id');
		$order = Model_Orders::find_by_pk($order_id);
		$order_info = $order->to_array();

		$data['order'] = $order_info;
		$data['order']['charge'] = unserialize($data['order']['charge']);
		$data['order']['cart_items'] = unserialize($data['order']['cart_items']);
		$data['order']['timestamp'] = date("F j, Y", strtotime($data['order']['timestamp']));
		
		$this->template->title = 'Vizzy Pop - View Order';	
		$this->template->content = View::forge('product/view_order', $data, false);
	}


	public function action_delete()
	{
		$id = str_replace('.','', Input::post('id'));
		Session::delete('cart.'.$id);
		Response::redirect(Input::referrer());
	}

	
	public function action_test()
	{
		$contents = File::read_dir(APPPATH.DS.'uploads'.DS.'tmp');

		foreach($contents as $file)
			echo $file .' - '. File::get_time(APPPATH.DS.'uploads'.DS.'tmp'.DS.$file, 'created');
		
		$this->template->title = '';
		$this->template->content = '';
	}

	public function make_cart()
	{
		$cart_items = Session::get('cart');
		if(is_array($cart_items))
		{
			
		}
		return View::forge('product/cart', array('cart_items' => $cart_items));
	}


}
