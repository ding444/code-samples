# Miscellaneous Scripts

## Introduction

These are a few files I've chosen to include that demonstrate different things.  Some demonstrate my more current ES6 styling.

- **PhraseUtil.js** - This is a tool to get random phrases from a list of intents (i.e. select random from `['hi', 'hello', 'good morning', 'hey']`) for a chatbot I've been building.
- **IntroductionUtil.js** - This is a chatbot library that checks for new users with functionality for performing a DISC psychometric analysis mashed into it.  Really these should be separated out but it was done as a proof of concept and I had just a few hours to get it done.  It ouputs the text in embarassingly ugly ASCII text.
- **ConversationUtil.js** - A library for handling some conversation logic for a custom chatbot engine.
- **server.js** - The backend for a simple, single-paged website that has an email signup form.
- **product.php** - A PHP file from one of my PHP projects
- **sql/\*** - Miscellaneous MySQL stored scripts

