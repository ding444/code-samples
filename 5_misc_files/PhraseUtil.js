/*****************************************
 * question/phrase array utility
 * phrases located in /lib/phrases.yml
 *****************************************/

require('require-yaml');
const async = require('async');
const phrases = require('./phrases.yml');

class PhraseUtil {
	constructor() {
		this.phrases = phrases;
	}
	
	get_random_phrase(phrase_pool) {
		// return phrase_pool[Math.floor(Math.random() * phrase_pool.length)];
		if(typeof phrase_pool == 'string')
			phrase_pool = this.phrases[phrase_pool];

		let total_weight = phrase_pool.reduce((prev, cur) => {
			return prev + cur.weight;
		}, 0);

		var random_num = this.get_rand(0, total_weight);
		var weight_sum = 0;

		for (var i = 0; i < phrase_pool.length; i++) {
			weight_sum += phrase_pool[i].weight;
			weight_sum = +weight_sum.toFixed(2);
			
			if (random_num <= weight_sum) {
				return phrase_pool[i].phrase;
			}
		}
	}

	get_rand(min, max) {
		return Math.random() * (max - min) + min;
	}

	eradicate_human_life(bot,message) {
		async.eachSeries(this.phrases.eradicate_egg, (phrase, callback) => {
			bot.reply(message, phrase);	
			setTimeout(()=>{
				callback(null);
			},((Math.random() * 4)+2)*1000);
		},(err) => {
			if(err) console.log(err);
		});
	}
}

module.exports = new PhraseUtil;
