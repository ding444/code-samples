'use strict';

const mysql = require('mysql');
const settings = require('../settings.js');

const mysql_connection = mysql.createConnection({
  user: settings.mysql.user,
  password: settings.mysql.password,
  database: settings.mysql.database,
});

class Conversation {
  constructor() {}

  create(data, msg, callback) {
    mysql_connection.query('INSERT INTO conversations SET ?', data, (err, result) => {
      if(err) console.error(err);
      const sql = 'SELECT a.*, b.library_file_name FROM conversations AS a LEFT JOIN conversation_types AS b ON a.conversation_type = b.id WHERE a.id = ?';
      mysql_connection.query(sql, [result.insertId], (err, rows, fields) => {
        const conversation = rows[0];
        this.continue(msg, conversation, callback);
      });
      //result.insertId;
    });
  }

  get(user_id, channel_id, callback) {
    const sql = 'SELECT a.*, b.library_file_name FROM conversations AS a LEFT JOIN conversation_types AS b ON a.conversation_type = b.id WHERE a.user_id = ? AND a.channel_id = ? AND a.is_finished = ?';
    mysql_connection.query(sql, [user_id, channel_id, 0], (err, rows, fields) => {
      if(err) console.error(err);

      if(rows.length < 1)
        rows = false;

      callback(rows[0]);
    });
  }

  continue(msg, conversation, callback) {
    // Continue existing conversation
    if(conversation.library_file_name !== null) {
      // Load and initialize specified conversation library
      const controller = require(`./conversations/${conversation.library_file_name}`)(conversation);
      const phase = controller.phases[conversation.phase];
      // Validate reply
      phase(msg, (error, response) => {
        // If failed validation return error.msg
        if(error)
          callback(error);
        else {
          // get next message
          callback(null, response);
        }
      });
    }
    else {
      console.err('Invalid conversation type');
      callback({message: 'Error: Invalid conversation type'});
    }
  }

  end(converation_id, finish_result = 'complete') {
    const sql = 'UPDATE conversations SET is_finished = 1, finish_result = ? WHERE id = ?';
    mysql_connection.query(sql, [finish_result, conversation_id], (err, result) => {
      if(err) console.error(err);
    });
  }
}

module.exports = Conversation;
